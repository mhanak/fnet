/**************************************************************************
* 
* Copyright 2019 by Michal Hanak. FNET Community.
* Copyright 2012-2013 by Andrey Butok. FNET Community.
* Copyright 2005-2009 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived ffatfs or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement ffatfs your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_fs_fatfs.h
*
* @author Michal Hanak
*
* @date May-16-2019
*
* @version 0.0.0.0
*
* @brief FNET FATFS FS API for FatFS.
*
***************************************************************************/
#ifndef _FNET_FS_FATFS_H_

#define _FNET_FS_FATFS_H_

#include "fnet_config.h"

#if (FNET_CFG_FS && FNET_CFG_FS_FATFS)

#include "fnet_fs.h"

/*! @addtogroup fnet_fs_fatfs
* The FNET FATFS File System provides read-write access to files 
* and directories. @n
* @n
* Configuration parameters:
* - @ref FNET_CFG_FS_FATFS  
*/

/*! @{ */

/**************************************************************************/ /*!
 * @brief FNET FATFS file-system name string.
 ******************************************************************************/ 
#define FNET_FS_FATFS_NAME     "fnet_fatfs"

/**************************************************************************/ /*!
 * @brief FNET FATFS file-system current version.
 ******************************************************************************/ 
#define FNET_FS_FATFS_VERSION  (1)

/**************************************************************************/ /*!
 * @brief FNET FATFS file-system maximum simultaneously open directories.
 ******************************************************************************/
#ifndef FNET_CFG_FS_FATFS_ODIR_MAX
	#define FNET_CFG_FS_FATFS_ODIR_MAX (FNET_CFG_FS_DESC_MAX)
#endif

/**************************************************************************/ /*!
 * @brief FNET FATFS file-system maximum simultaneously open files.
 ******************************************************************************/
#ifndef FNET_CFG_FS_FATFS_OFILE_MAX
	#define FNET_CFG_FS_FATFS_OFILE_MAX (FNET_CFG_FS_DESC_MAX)
#endif

/***************************************************************************/ /*!
 *
 * @brief    Registers the FNET FATFS file system.
 *
 * @see fnet_fs_fatfs_unregister()
 * 
 ******************************************************************************
 *
 * This function registers the FNET FATFS file system within the FNET FS interface. 
 *
 ******************************************************************************/
void fnet_fs_fatfs_register(void);

/***************************************************************************/ /*!
 *
 * @brief    Unregisters the FNET FATFS file system.
 *
 * @see fnet_fs_fatfs_register()
 *
 ******************************************************************************
 *
 * This function unregisters the FNET FATFS file system ffatfs the FNET FS interface. 
 *
 ******************************************************************************/
void fnet_fs_fatfs_unregister(void);

/*! @} */

#endif /* FNET_CFG_FS && FNET_CFG_FS_FATFS */

#endif /* _FNET_FS_FATFS_H_ */
