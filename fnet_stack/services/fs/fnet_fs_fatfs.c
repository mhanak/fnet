/**************************************************************************
* 
* Copyright 2019 by Michal Hanak. FNET Community.
* Copyright 2012-2013 by Andrey Butok. FNET Community.
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived ffatfs or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement ffatfs your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_fs_fatfs.c
*
* @author Michal Hanak
*
* @date May-16-2019
*
* @version 0.0.0.0
*
* @brief FNET FATFS FS API for FatFS.
*
***************************************************************************/

#include "fnet_config.h"

#if FNET_CFG_FS && FNET_CFG_FS_FATFS

#include "fnet_fs_fatfs.h"
#include "fnet_fs_prv.h"
#include "fnet.h"

/* FatFS main include file, user is responsible for configuring it in ffconf.h */
#include "ff.h"

int fnet_fs_fatfs_opendir(struct fnet_fs_desc *dir, const char *name);
int fnet_fs_fatfs_readdir(struct fnet_fs_desc *dir, struct fnet_fs_dirent* dirent);
int fnet_fs_fatfs_rewinddir(struct fnet_fs_desc *dir);
int fnet_fs_fatfs_closedir(struct fnet_fs_desc *dir);
int fnet_fs_fatfs_fopen(struct fnet_fs_desc *file, const char *name, char mode, struct fnet_fs_desc * re_dir);
unsigned long fnet_fs_fatfs_fread(struct fnet_fs_desc *file, char * buf, unsigned long bytes);
int fnet_fs_fatfs_mount(void *arg);
int fnet_fs_fatfs_fseek(struct fnet_fs_desc *file, long offset, fnet_fs_seek_origin_t origin) ;
int fnet_fs_fatfs_finfo(struct fnet_fs_desc *file, struct fnet_fs_dirent *info);
int fnet_fs_fatfs_fclose(struct fnet_fs_desc *file);

/* FS  directory operations */
static const struct fnet_fs_dir_operations fnet_fs_fatfs_dir_operations =
{
    fnet_fs_fatfs_opendir,
    fnet_fs_fatfs_readdir,
    fnet_fs_fatfs_rewinddir,
    fnet_fs_fatfs_closedir
};

/* FS  file operations */
static const struct fnet_fs_file_operations fnet_fs_fatfs_file_operations =
{
    fnet_fs_fatfs_fopen,
    fnet_fs_fatfs_fread,
    fnet_fs_fatfs_fseek,
    fnet_fs_fatfs_finfo,
    fnet_fs_fatfs_fclose
};

/* FS operations */
static const struct fnet_fs_operations fnet_fs_fatfs_operations =
{
    fnet_fs_fatfs_mount
};

/* FS interface structure */
static struct fnet_fs fnet_fs_fatfs =
{
    FNET_FS_FATFS_NAME,
    &fnet_fs_fatfs_operations,
    &fnet_fs_fatfs_file_operations,
    &fnet_fs_fatfs_dir_operations,
    0,
    0
};

static int fnet_fs_fatfs_registered;  /* Flag that FATFS FS is registered or not.*/

/* table of open directory and file entries */
static DIR fnet_fs_odir[FNET_CFG_FS_FATFS_ODIR_MAX];
static FIL fnet_fs_ofil[FNET_CFG_FS_FATFS_OFILE_MAX];

/************************************************************************
* static helper functions
*************************************************************************/

/* helper to retrieve FATFS pointer from file object */
static inline FATFS* fnet_fs_fatfs_get(struct fnet_fs_desc *f)
{
	return (f && f->mount) ? f->mount->arg : NULL;
}

/* helper to find free DIR entry */
static DIR* fnet_fs_fatfs_new_odir(void)
{
	int i;
	DIR* d;

	for(i=0; i<FNET_CFG_FS_FATFS_ODIR_MAX; i++)
	{
		d = &fnet_fs_odir[i];

		if(!d->obj.fs)
			return d;
	}

	return NULL;
}

/* helper to invalidate DIR entry */
static void fnet_fs_fatfs_del_odir(DIR* d)
{
	fnet_memset_zero(d, sizeof(DIR));
}

/* helper to find free FIL entry */
static FIL* fnet_fs_fatfs_new_ofil(void)
{
	int i;
	FIL* f;

	for(i=0; i<FNET_CFG_FS_FATFS_OFILE_MAX; i++)
	{
		f = &fnet_fs_ofil[i];

		if(!f->obj.fs)
			return f;
	}

	return NULL;
}

/* helper to invalidate FIL entry */
static void fnet_fs_fatfs_del_ofil(FIL* f)
{
	fnet_memset_zero(f, sizeof(FIL));
}

static inline unsigned long fsize_saturate_long(FSIZE_t size)
{
	if(size > 0xffffffffUL)
		return 0xffffffffUL;
	else
		return (unsigned long)size;
}

/************************************************************************
* NAME: fnet_fs_fatfs_register
*
* DESCRIPTION: This function registers the FATFS FS.
*************************************************************************/
void fnet_fs_fatfs_register( void )
{
    if(fnet_fs_fatfs_registered++ == 0)
    {
    	fnet_memset_zero(&fnet_fs_odir, sizeof(fnet_fs_odir));
    	fnet_memset_zero(&fnet_fs_ofil, sizeof(fnet_fs_ofil));

        fnet_fs_register(&fnet_fs_fatfs);
    }
}

/************************************************************************
* NAME: fnet_fs_fatfs_unregister
*
* DESCRIPTION: This function unregisters the FATFS FS.
*************************************************************************/
void fnet_fs_fatfs_unregister( void )
{
    if(--fnet_fs_fatfs_registered == 0)
        fnet_fs_unregister(&fnet_fs_fatfs);
}

/************************************************************************
* NAME: fnet_fs_fatfs_mount
*
* DESCRIPTION:
*************************************************************************/
int fnet_fs_fatfs_mount( void *arg )
{
    /* the arg is a pointer to FATFS structure, must be non-NULL (cannot validate it more) */
    return arg ? FNET_OK : FNET_ERR;
}

/************************************************************************
* NAME: fnet_fs_fatfs_opendir
*
* DESCRIPTION: Open DIR stream for the FATFS FS.
*************************************************************************/
int fnet_fs_fatfs_opendir(struct fnet_fs_desc *dir, const char *name)
{
    int result = FNET_ERR;
    
    if(dir && name)
    {
    	/* get new DIR entry */
    	DIR* odir = fnet_fs_fatfs_new_odir();

    	if(odir)
    	{
    		if(f_opendir(odir, name) == FR_OK)
    		{
    			/* bind DIR to the file descriptor */
    			dir->id = (unsigned long) odir;
    			dir->pos = 0;
    			result = FNET_OK;
    		}
    		else
    		{
    			fnet_fs_fatfs_del_odir(odir);
    		}
    	}
    }

    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_readdir
*
* DESCRIPTION: Read DIR stream for the FATFS FS.
*************************************************************************/
int fnet_fs_fatfs_readdir(struct fnet_fs_desc *dir, struct fnet_fs_dirent* dirent)
{
	static FILINFO info;
    int result = FNET_ERR;

    if(dir && dir->id && dirent)
	{
		DIR* odir = (DIR*)dir->id;

		fnet_memset_zero(&info, sizeof(info));
		fnet_memset_zero(dirent, sizeof(*dirent));

		/* Note that FatFS may return OK even if no entry is filled. */
		if(f_readdir(odir, &info) == FR_OK && info.fname[0])
		{
			dirent->d_ino = 0; /* don't know how to get this */
			dirent->d_type = (info.fattrib & AM_DIR) ? DT_DIR : DT_REG;
			dirent->d_name = info.fname;
			dirent->d_size = fsize_saturate_long(info.fsize);
			dir->pos++;
			result = FNET_OK;
		}
		else
		{
			dir->pos = FNET_FS_EOF;
		}
	}
    
    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_closedir
*
* DESCRIPTION: Close DIR stream for the FATFS FS.
*************************************************************************/
int fnet_fs_fatfs_closedir(struct fnet_fs_desc *dir)
{
    int result = FNET_ERR;

    if(dir && dir->id)
	{
		DIR* odir = (DIR*)dir->id;

		dir->id = 0;
		f_closedir(odir);
		fnet_fs_fatfs_del_odir(odir);

    	result = FNET_OK;
	}

    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_rewinddir
*
* DESCRIPTION: Rewind/reopen DIR stream for the FATFS FS.
*************************************************************************/
int fnet_fs_fatfs_rewinddir(struct fnet_fs_desc *dir)
{
    int result = FNET_ERR;

    if(dir && dir->id)
	{
		DIR* odir = (DIR*)dir->id;

		/* This is the way how FatFS does rewinddir */
		if(f_readdir(odir, NULL) == FR_OK)
		{
			dir->pos = 0;
			result = FNET_OK;
		}
	}

    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_fopen
*
* DESCRIPTION: Open FILE stream for the FATFS FS.
*************************************************************************/
int fnet_fs_fatfs_fopen( struct fnet_fs_desc *file, const char *name, char mode, struct fnet_fs_desc * re_dir )
{
    int result = FNET_ERR;
    
    if(file && name && mode == (FNET_FS_MODE_READ|FNET_FS_MODE_OPEN_EXISTING))
    {
    	/* get new FIL entry */
    	FIL* ofil = fnet_fs_fatfs_new_ofil();

    	if(ofil)
    	{
    		if(f_open(ofil, name, FA_READ) == FR_OK)
    		{
    			/* bind FIL to the file descriptor */
    			file->id = (unsigned long) ofil;
    			file->pos = 0;
    			result = FNET_OK;
    		}
    		else
    		{
    			fnet_fs_fatfs_del_ofil(ofil);
    		}
    	}
    }
        
    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_fclose
*
* DESCRIPTION:
*************************************************************************/
int fnet_fs_fatfs_fclose(struct fnet_fs_desc *file)
{
    int result = FNET_ERR;

    if(file && file->id)
    {
    	FIL* ofil = (FIL*)file->id;

    	file->id = 0;
    	f_close(ofil);
		fnet_fs_fatfs_del_ofil(ofil);

		result = FNET_OK;
    }

    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_fread
*
* DESCRIPTION: 
*************************************************************************/
unsigned long fnet_fs_fatfs_fread(struct fnet_fs_desc *file, char * buf, unsigned long bytes)
{
    unsigned long result = 0;
    
    if(file && file->id && buf && bytes)
    {
    	FIL* ofil = (FIL*)file->id;

    	UINT br = 0;
    	if(f_read(ofil, buf, bytes, &br) == FR_OK)
    	{
    		file->pos += br;
            result = (unsigned long)br;

            /* end of file reached? */
            if(!br)
    			file->pos = FNET_FS_EOF;
    	}
    }
    
    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_fseek
*
* DESCRIPTION:
*************************************************************************/
int fnet_fs_fatfs_fseek(struct fnet_fs_desc *file, long offset, fnet_fs_seek_origin_t origin)
{
    int result = FNET_ERR;

    if(file && file->id)
    {
    	FIL* ofil = (FIL*)file->id;
    	unsigned long new_pos = (unsigned long)ofil->fptr;

		switch(origin)
		{
			case FNET_FS_SEEK_SET:
				new_pos = offset;
				break;
			case FNET_FS_SEEK_CUR:
				new_pos += offset;
				break;
			case FNET_FS_SEEK_END:
				new_pos = (unsigned long)(ofil->obj.objsize - 1 - offset);
				break;
			default:
				new_pos = FNET_FS_EOF;
		}

		FRESULT fr = FR_OK;
		if(new_pos >= 0)
			fr = f_lseek(ofil, new_pos);

		if(fr == FR_OK)
		{
			file->pos = new_pos;
			result = FNET_OK;
        }
    }
    
    return result;
}

/************************************************************************
* NAME: fnet_fs_fatfs_finfo
*
* DESCRIPTION:
*************************************************************************/
int fnet_fs_fatfs_finfo(struct fnet_fs_desc *file, struct fnet_fs_dirent *dirent)
{
    int result = FNET_ERR;

    if(file && file->id && dirent)
    {
    	FIL* ofil = (FIL*)file->id;

    	fnet_memset_zero(dirent, sizeof(*dirent));

    	dirent->d_name = ""; /* can't retrieve this */
    	dirent->d_size = ofil->obj.objsize;
        result = FNET_OK;
    }
    
    return result;
}

#endif
