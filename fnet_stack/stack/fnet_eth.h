/**************************************************************************
* 
* Copyright 2012-2013 by Andrey Butok. FNET Community.
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_eth.h
*
* @author Andrey Butok
*
* @date Dec-19-2012
*
* @version 0.1.9.0
*
* @brief Ethernet platform independent API definitions.
*
***************************************************************************/

#ifndef _FNET_ETH_H_

#define _FNET_ETH_H_

#include "fnet_config.h"

/************************************************************************
*     Standard MII Register Indexes.
*************************************************************************/
#define FNET_ETH_MII_REG_CR              (0)   /* Control Register */
#define FNET_ETH_MII_REG_SR              (1)   /* Status Register */
#define FNET_ETH_MII_REG_IDR1            (2)   /* Identification Register #1 */
#define FNET_ETH_MII_REG_IDR2            (3)   /* Identification Register #2 */
#define FNET_ETH_MII_REG_AN_ADVERT       (4)   /* Auto-Negotiation Advertisement Register */
#define FNET_ETH_MII_REG_AN_PABILS1      (5)   /* Auto-Negotiation Link Partner Ability Register #1 */
#define FNET_ETH_MII_REG_AN_ER           (6)   /* Auto-Negotiation Expansion Register */
#define FNET_ETH_MII_REG_AN_NEXT_PGTX    (7)   /* Auto-Negotiation Next Page TX Register */
#define FNET_ETH_MII_REG_AN_PABILS2 	 (8)   /* Auto-Negotiation Link Partner Ability Register #2 */
#define FNET_ETH_MII_REG_AN_ADVERTG      (9)   /* Auto-Negotiation Advertisement Register for gig */
#define FNET_ETH_MII_REG_AN_PABILS3 	(10)   /* Auto-Negotiation Link Partner Ability Register #3 */
#define FNET_ETH_MII_REG_EXTSR          (15)   /* Extended Status Register */
#define FNET_ETH_MII_REG_VENDOR_CR      (16)
#define FNET_ETH_MII_REG_VENDOR_SR      (17)
#define FNET_ETH_MII_REG_VENDOR_SR2     (19)
#define FNET_ETH_MII_REG_RXERR_CTR      (21)

/* Status Register (1) */
#define FNET_ETH_MII_REG_SR_LINK_STATUS (0x0004)
#define FNET_ETH_MII_REG_SR_AN_ABILITY  (0x0008)
#define FNET_ETH_MII_REG_SR_AN_COMPLETE (0x0020)
#define FNET_ETH_MII_REG_SR_EXTSTAT     (0x0100)


/* Control Register (0) */
#define FNET_ETH_MII_REG_CR_RESET       (0x8000)    /* Resetting a port is accomplished by setting this bit to 1.*/
#define FNET_ETH_MII_REG_CR_LOOPBACK    (0x4000)    /* Determines Digital Loopback Mode. */
#define FNET_ETH_MII_REG_CR_SPEED_MASK  (0x2040)    /* Speed (two-bit value) */
#define FNET_ETH_MII_REG_CR_SPEED_1000M (0x0040)
#define FNET_ETH_MII_REG_CR_SPEED_100M  (0x2000)
#define FNET_ETH_MII_REG_CR_SPEED_10M   (0x0000)
#define FNET_ETH_MII_REG_CR_AN_ENABLE   (0x1000)    /* Auto-Negotiation Enable bit. */
#define FNET_ETH_MII_REG_CR_PDWN        (0x0800)    /* Power Down bit. */
#define FNET_ETH_MII_REG_CR_ISOL        (0x0400)    /* Isolate bit.*/
#define FNET_ETH_MII_REG_CR_AN_RESTART  (0x0200)    /* Restart Auto-Negotiation bit.*/
#define FNET_ETH_MII_REG_CR_DPLX        (0x0100)    /* Duplex Mode bit.*/

/* Auto-Negotiation Advertisement Register (4) */
#define FNET_ETH_MII_REG_ANADV_ASYMMETRIC_PAUSE (0x0800)
#define FNET_ETH_MII_REG_ANADV_PAUSE            (0x0400)
#define FNET_ETH_MII_REG_ANADV_10HALF           (0x0020)  /* Try for 10mbps half-duplex  */
#define FNET_ETH_MII_REG_ANADV_10FULL           (0x0040)  /* Try for 10mbps full-duplex  */
#define FNET_ETH_MII_REG_ANADV_100HALF          (0x0080)  /* Try for 100mbps half-duplex */
#define FNET_ETH_MII_REG_ANADV_100FULL          (0x0100)  /* Try for 100mbps full-duplex */
#define FNET_ETH_MII_REG_ANADV_10               (FNET_ETH_MII_REG_ANADV_10HALF|FNET_ETH_MII_REG_ANADV_10FULL)
#define FNET_ETH_MII_REG_ANADV_100              (FNET_ETH_MII_REG_ANADV_100HALF|FNET_ETH_MII_REG_ANADV_100FULL)

/* Auto-Negotiation Advertisement Register for gig (9) */
#define FNET_ETH_MII_REG_ANADVG_1000HALF		(0x0100)
#define FNET_ETH_MII_REG_ANADVG_1000FULL		(0x0200)

/* Auto-Negotiation Link Partner Ability Register #1 (5) */
#define FNET_ETH_MII_REG_PABILS1_ABILITY_MASK   (0x1FE0)
#define FNET_ETH_MII_REG_PABILS1_100T4          (0x0200)
#define FNET_ETH_MII_REG_PABILS1_100FULL        (0x0100)
#define FNET_ETH_MII_REG_PABILS1_100HALF        (0x0080)
#define FNET_ETH_MII_REG_PABILS1_10FULL         (0x0040)
#define FNET_ETH_MII_REG_PABILS1_10HALF         (0x0020)

/* Auto-Negotiation Link Partner Ability Register #3 (10) */
#define FNET_ETH_MII_REG_PABILS3_1000HALF       (0x0400)
#define FNET_ETH_MII_REG_PABILS3_1000FULL       (0x0800)


//#define FNET_ETH_MII_REG_ANADV_ERROR            0x8000

//#define FNET_ETH_MII_REG_ANAR_NEXT_PAGE (0x8000)

#define FNET_ETH_MII_TIMEOUT            (0x10000)   /* Timeout counter for MII communications.*/

/**************************************************************************
*     FNET-defined flags to describe auto-negotiated capabilities
***************************************************************************/

#define FNET_ETH_PHY_MEDIA_10HALF     (0x0001)
#define FNET_ETH_PHY_MEDIA_10FULL     (0x0002)
#define FNET_ETH_PHY_MEDIA_10         (FNET_ETH_PHY_MEDIA_10HALF|FNET_ETH_PHY_MEDIA_10FULL)
#define FNET_ETH_PHY_MEDIA_100HALF    (0x0004)
#define FNET_ETH_PHY_MEDIA_100FULL    (0x0008)
#define FNET_ETH_PHY_MEDIA_100        (FNET_ETH_PHY_MEDIA_100HALF|FNET_ETH_PHY_MEDIA_100FULL)
#define FNET_ETH_PHY_MEDIA_1000HALF   (0x0010)
#define FNET_ETH_PHY_MEDIA_1000FULL   (0x0020)
#define FNET_ETH_PHY_MEDIA_1000       (FNET_ETH_PHY_MEDIA_1000HALF|FNET_ETH_PHY_MEDIA_1000FULL)
#define FNET_ETH_PHY_MEDIA_PAUSE      (0x1000)
#define FNET_ETH_PHY_MEDIA_ASYM_PAUSE (0x2000)
#define FNET_ETH_PHY_MEDIA_INIT_RESET (0x8000) /* Indicates to assert SW PHY RESET prior to auto-negotiation */

/**************************************************************************
*     Definitions
***************************************************************************/

/*! @addtogroup fnet_netif
*/
/*! @{ */

/**************************************************************************/ /*!
 * @def     FNET_MAC_ADDR_STR_SIZE
 * @brief   Size of the string buffer that will contain 
 *          null-terminated ASCII string of MAC address, represented
 *          by six groups of two hexadecimal digits, separated by colons (:).
 * @see fnet_mac_to_str
 * @showinitializer 
 ******************************************************************************/
#define FNET_MAC_ADDR_STR_SIZE       (18)

/**************************************************************************/ /*!
 * @brief Media Access Control (MAC) address  type.
 ******************************************************************************/
typedef unsigned char fnet_mac_addr_t[6]; /* MAC address type.*/


/******************************************************************************
*     Function Prototypes
*******************************************************************************/

/***************************************************************************/ /*!
 *
 * @brief    Converts a 6 byte MAC address into a null terminated string.
 *
 *
 * @param addr       MAC address.
 *
 * @param str_mac    Pointer to a character buffer will contain the resulting 
 *                   text address in standard ":" notation. @n 
 *                   The @c str_mac buffer must be at least 18 bytes long 
 *                   (@ref FNET_MAC_ADDR_STR_SIZE). 
 *
 * @see fnet_str_to_mac()
 ******************************************************************************
 * This function takes an MAC-48 address, specified by the @c addr 
 * parameter, and returns a null-terminated ASCII string, represented
 * by six groups of two hexadecimal digits, separated by colons (:), 
 * in transmission order (e.g. 01:23:45:67:89:ab ), into buffer pointed to by the
 * @c str_mac.
 ******************************************************************************/
void fnet_mac_to_str( fnet_mac_addr_t addr, char *str_mac );

/***************************************************************************/ /*!
 *
 * @brief    Converts a null terminated string to a 6 byte MAC address 
 *
 * @param str_mac    Null-terminated MAC address string as pairs of 
 *                   hexadecimal numbers separated by colons.
 *
 * @param addr       Buffer will contain a suitable 
 *                   binary representation of the @c str_mac MAC address .
 *
 * @return This function returns:
 *   - @ref FNET_OK if no error occurs.
 *   - @ref FNET_ERR if the string in the @c str_mac parameter does not contain 
 *     a legitimate MAC address.
 *
 * @see fnet_mac_to_str()
 ******************************************************************************
 * This function interprets the character string specified by the @c str_mac 
 * parameter. This string represents a numeric MAC address expressed 
 * in six groups of two hexadecimal digits, separated by colons (:), 
 * in transmission order. The value returned, pointed to by the @c addr,
 * is a number suitable for use as an MAC address.
 ******************************************************************************/
int fnet_str_to_mac( char *str_mac, fnet_mac_addr_t addr );

/*! @} */

#endif
