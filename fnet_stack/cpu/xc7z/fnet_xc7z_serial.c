/**************************************************************************
* 
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_XC7Z_serial.c
*
* @author Andrey Butok
*
* @date Oct-26-2012
*
* @version 0.0.8.0
*
* @brief Kinetis Serial port I/O functions.
*
***************************************************************************/

#include "fnet_config.h"

#if FNET_XC7Z

#include "fnet.h"

XUartPs*  fnet_xc7z_uart[FNET_CFG_CPU_SERIAL_PORT_MAX+1] = { NULL };

/********************************************************************/

void fnet_cpu_serial_init(long port_number, unsigned long baud_rate)
{
    static XUartPs fnet_xc7z_uart_instance[FNET_CFG_CPU_SERIAL_PORT_MAX+1];
    XUartPs_Config* pcfg;

    FNET_ASSERT(port_number >= 0 && port_number <= FNET_CFG_CPU_SERIAL_PORT_MAX);

    /* user might have set another UART instance before starting FNET */
    if(!fnet_xc7z_uart[port_number])
    {
        pcfg = XUartPs_LookupConfig(port_number);
        XUartPs_CfgInitialize(&fnet_xc7z_uart_instance[port_number], pcfg, pcfg->BaseAddress);
        fnet_xc7z_uart[port_number] = &fnet_xc7z_uart_instance[port_number];
    }

    if(fnet_xc7z_uart[port_number])
    {
        u16 opt;
        XUartPsFormat fmt;

        fmt.BaudRate = baud_rate;
        fmt.DataBits = XUARTPS_FORMAT_8_BITS;
        fmt.Parity   = XUARTPS_FORMAT_NO_PARITY;
        fmt.StopBits = XUARTPS_FORMAT_1_STOP_BIT;

        XUartPs_SetDataFormat(fnet_xc7z_uart[port_number], &fmt);
        opt = XUartPs_GetOptions(fnet_xc7z_uart[port_number]);
        XUartPs_SetOptions(fnet_xc7z_uart[port_number], opt & ~XUARTPS_OPTION_SET_FCM);
        XUartPs_SetInterruptMask(fnet_xc7z_uart[port_number], 0);
        XUartPs_EnableUart(fnet_xc7z_uart[port_number]);
    }
}

/********************************************************************/
void fnet_cpu_serial_putchar (long port_number, int character)
{
    u8 c = (u8) character;;

    FNET_ASSERT(port_number >= 0 && port_number <= FNET_CFG_CPU_SERIAL_PORT_MAX);

    if(fnet_xc7z_uart[port_number])
    {
        while(XUartPs_Send(fnet_xc7z_uart[port_number], &c, 1) == 0)
            ; /* wait until we push the character into fifo */
    }
}

/********************************************************************/
int fnet_cpu_serial_getchar (long port_number)
{
    u8 buff[4];

    FNET_ASSERT(port_number >= 0 && port_number <= FNET_CFG_CPU_SERIAL_PORT_MAX);

    if(fnet_xc7z_uart[port_number])
        if(XUartPs_Recv(fnet_xc7z_uart[port_number], &buff[0], 1) > 0)
            return (int) buff[0];

    return FNET_ERR;
}

#endif /*FNET_XC7Z*/
