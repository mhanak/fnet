/**************************************************************************
*
* Copyright 2013
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based
* on this library.
* If you modify the FNET sources, you may extend this exception
* to your version of the FNET sources, but you are not obligated
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_xc7z_eth.h
*
* @author Michal Hanak
*
* @date Aug-2-2012
*
* @version 0.1.11.0
*
* @brief XC7Z EMAC Registers
*
***************************************************************************/

#ifndef _FNET_XC7Z_REGS_H_
#define _FNET_XC7Z_REGS_H_

typedef volatile struct
{
    u32 net_ctrl;             /* 0x00000000 (mixed): Network Control, reset=0x00000000 */
    u32 net_cfg;              /* 0x00000004 (rw): Network Configuration, reset=0x00080000 */
    u32 net_status;           /* 0x00000008 (ro): Network Status */
    u32 reserved1[1];         /* 0x0000000c (no): reserved area */
    u32 dma_cfg;              /* 0x00000010 (mixed): DMA Configuration, reset=0x00020784 */
    u32 tx_status;            /* 0x00000014 (mixed): Transmit Status, reset=0x00000000 */
    u32 rx_qbar;              /* 0x00000018 (mixed): Receive Buffer Queue Base Address, reset=0x00000000 */
    u32 tx_qbar;              /* 0x0000001C (mixed): Transmit Buffer Queue Base Address, reset=0x00000000 */
    u32 rx_status;            /* 0x00000020 (mixed): Receive Status, reset=0x00000000 */
    u32 intr_status;          /* 0x00000024 (mixed): Interrupt Status, reset=0x00000000 */
    u32 intr_en;              /* 0x00000028 (wo): Interrupt Enable */
    u32 intr_dis;             /* 0x0000002C (wo): Interrupt Disable */
    u32 intr_mask;            /* 0x00000030 (mixed): Interrupt Mask Status */
    u32 phy_maint;            /* 0x00000034 (rw): PHY Maintenance, reset=0x00000000 */
    u32 rx_pauseq;            /* 0x00000038 (ro): Received Pause Quantum, reset=0x00000000 */
    u32 tx_pauseq;            /* 0x0000003C (rw): Transmit Pause Quantum, reset=0x0000FFFF */
    u32 reserved2[16];        /* 0x00000040 (no): reserved area */
    u32 hash_bot;             /* 0x00000080 (rw): Hash Register Bottom [31:0], reset=0x00000000 */
    u32 hash_top;             /* 0x00000084 (rw): Hash Register Top [63:32], reset=0x00000000 */
    u32 spec_addr1_bot;       /* 0x00000088 (rw): Specific Address 1 Bottom [31:0], reset=0x00000000 */
    u32 spec_addr1_top;       /* 0x0000008C (mixed): Specific Address 1 Top [47:32], reset=0x00000000 */
    u32 spec_addr2_bot;       /* 0x00000090 (rw): Specific Address 2 Bottom [31:0], reset=0x00000000 */
    u32 spec_addr2_top;       /* 0x00000094 (mixed): Specific Address 2 Top [47:32], reset=0x00000000 */
    u32 spec_addr3_bot;       /* 0x00000098 (rw): Specific Address 3 Bottom [31:0], reset=0x00000000 */
    u32 spec_addr3_top;       /* 0x0000009C (mixed): Specific Address 3 Top [47:32], reset=0x00000000 */
    u32 spec_addr4_bot;       /* 0x000000A0 (rw): Specific Address 4 Bottom [31:0], reset=0x00000000 */
    u32 spec_addr4_top;       /* 0x000000A4 (mixed): Specific Address 4 Top [47:32], reset=0x00000000 */
    u32 type_id_match1;       /* 0x000000A8 (mixed): Type ID Match 1, reset=0x00000000 */
    u32 type_id_match2;       /* 0x000000AC (mixed): Type ID Match 2, reset=0x00000000 */
    u32 type_id_match3;       /* 0x000000B0 (mixed): Type ID Match 3, reset=0x00000000 */
    u32 type_id_match4;       /* 0x000000B4 (mixed): Type ID Match 4, reset=0x00000000 */
    u32 wake_on_lan;          /* 0x000000B8 (mixed): Wake on LAN Register, reset=0x00000000 */
    u32 ipg_stretch;          /* 0x000000BC (mixed): IPG stretch register, reset=0x00000000 */
    u32 stacked_vlan;         /* 0x000000C0 (mixed): Stacked VLAN Register, reset=0x00000000 */
    u32 tx_pfc_pause;         /* 0x000000C4 (mixed): Transmit PFC Pause Register, reset=0x00000000 */
    u32 spec_addr1_mask_bot;  /* 0x000000C8 (rw): Specific Address Mask 1 Bottom [31:0], reset=0x00000000 */
    u32 spec_addr1_mask_top;  /* 0x000000CC (mixed): Specific Address Mask 1 Top [47:32], reset=0x00000000 */
    u32 reserved3[11];        /* 0x000000d0 (no): reserved area */
    u32 module_id;            /* 0x000000FC (ro): Module ID, reset=0x00020118 */
    u32 octets_tx_bot;        /* 0x00000100 (ro): Octets transmitted [31:0] (in frames without error), reset=0x00000000 */
    u32 octets_tx_top;        /* 0x00000104 (ro): Octets transmitted [47:32] (in frames without error), reset=0x00000000 */
    u32 frames_tx;            /* 0x00000108 (ro): Frames Transmitted, reset=0x00000000 */
    u32 broadcast_frames_tx;  /* 0x0000010C (ro): Broadcast frames Tx, reset=0x00000000 */
    u32 multi_frames_tx;      /* 0x00000110 (ro): Multicast frames Tx, reset=0x00000000 */
    u32 pause_frames_tx;      /* 0x00000114 (ro): Pause frames Tx, reset=0x00000000 */
    u32 frames_64b_tx;        /* 0x00000118 (ro): Frames Tx, 64-byte length, reset=0x00000000 */
    u32 frames_65to127b_tx;   /* 0x0000011C (ro): Frames Tx, 65 to 127-byte length, reset=0x00000000 */
    u32 frames_128to255b_tx;  /* 0x00000120 (ro): Frames Tx, 128 to 255-byte length, reset=0x00000000 */
    u32 frames_256to511b_tx;  /* 0x00000124 (ro): Frames Tx, 256 to 511-byte length, reset=0x00000000 */
    u32 frames_512to1023b_tx; /* 0x00000128 (ro): Frames Tx, 512 to 1023-byte length, reset=0x00000000 */
    u32 frames_1024to1518b_tx;/* 0x0000012C (ro): Frame Tx, 1024 to 1518-byte length, reset=0x00000000 */
    u32 reserved4[1];         /* 0x00000130 (no): reserved area */
    u32 tx_under_runs;        /* 0x00000134 (ro): Transmit under runs, reset=0x00000000 */
    u32 single_collisn_frames;/* 0x00000138 (ro): Single Collision Frames, reset=0x00000000 */
    u32 multi_collisn_frames; /* 0x0000013C (ro): Multiple Collision Frames, reset=0x00000000 */
    u32 excessive_collisns;   /* 0x00000140 (ro): Excessive Collisions, reset=0x00000000 */
    u32 late_collisns;        /* 0x00000144 (ro): Late Collisions, reset=0x00000000 */
    u32 deferred_tx_frames;   /* 0x00000148 (ro): Deferred Transmission Frames, reset=0x00000000 */
    u32 carrier_sense_errs;   /* 0x0000014C (ro): Carrier Sense Errors., reset=0x00000000 */
    u32 octets_rx_bot;        /* 0x00000150 (ro): Octets Received [31:0], reset=0x00000000 */
    u32 octets_rx_top;        /* 0x00000154 (ro): Octets Received [47:32], reset=0x00000000 */
    u32 frames_rx;            /* 0x00000158 (ro): Frames Received, reset=0x00000000 */
    u32 bdcast_fames_rx;      /* 0x0000015C (ro): Broadcast Frames Rx, reset=0x00000000 */
    u32 multi_frames_rx;      /* 0x00000160 (ro): Multicast Frames Rx, reset=0x00000000 */
    u32 pause_rx;             /* 0x00000164 (ro): Pause Frames Rx, reset=0x00000000 */
    u32 frames_64b_rx;        /* 0x00000168 (ro): Frames Rx, 64-byte length, reset=0x00000000 */
    u32 frames_65to127b_rx;   /* 0x0000016C (ro): Frames Rx, 65 to 127-byte length, reset=0x00000000 */
    u32 frames_128to255b_rx;  /* 0x00000170 (ro): Frames Rx, 128 to 255-byte length, reset=0x00000000 */
    u32 frames_256to511b_rx;  /* 0x00000174 (ro): Frames Rx, 256 to 511-byte length, reset=0x00000000 */
    u32 frames_512to1023b_rx; /* 0x00000178 (ro): Frames Rx, 512 to 1023-byte length, reset=0x00000000 */
    u32 frames_1024to1518b_rx;/* 0x0000017C (ro): Frames Rx, 1024 to 1518-byte length, reset=0x00000000 */
    u32 reserved5[1];         /* 0x00000180 (no): reserved area */
    u32 undersz_rx;           /* 0x00000184 (ro): Undersize frames received, reset=0x00000000 */
    u32 oversz_rx;            /* 0x00000188 (ro): Oversize frames received, reset=0x00000000 */
    u32 jab_rx;               /* 0x0000018C (ro): Jabbers received, reset=0x00000000 */
    u32 fcs_errors;           /* 0x00000190 (ro): Frame check sequence errors, reset=0x00000000 */
    u32 length_field_errors;  /* 0x00000194 (ro): Length field frame errors, reset=0x00000000 */
    u32 rx_symbol_errors;     /* 0x00000198 (ro): Receive symbol errors, reset=0x00000000 */
    u32 align_errors;         /* 0x0000019C (ro): Alignment errors, reset=0x00000000 */
    u32 rx_resource_errors;   /* 0x000001A0 (ro): Receive resource errors, reset=0x00000000 */
    u32 rx_overrun_errors;    /* 0x000001A4 (ro): Receive overrun errors, reset=0x00000000 */
    u32 ip_hdr_csum_errors;   /* 0x000001A8 (ro): IP header checksum errors, reset=0x00000000 */
    u32 tcp_csum_errors;      /* 0x000001AC (ro): TCP checksum errors, reset=0x00000000 */
    u32 udp_csum_errors;      /* 0x000001B0 (ro): UDP checksum error, reset=0x00000000 */
    u32 reserved6[5];         /* 0x000001b4 (no): reserved area */
    u32 timer_strobe_s;       /* 0x000001C8 (rw): 1588 timer sync strobe seconds, reset=0x00000000 */
    u32 timer_strobe_ns;      /* 0x000001CC (mixed): 1588 timer sync strobe nanoseconds, reset=0x00000000 */
    u32 timer_s;              /* 0x000001D0 (rw): 1588 timer seconds, reset=0x00000000 */
    u32 timer_ns;             /* 0x000001D4 (mixed): 1588 timer nanoseconds, reset=0x00000000 */
    u32 timer_adjust;         /* 0x000001D8 (mixed): 1588 timer adjust, reset=0x00000000 */
    u32 timer_incr;           /* 0x000001DC (mixed): 1588 timer increment, reset=0x00000000 */
    u32 ptp_tx_s;             /* 0x000001E0 (ro): PTP event frame transmitted seconds, reset=0x00000000 */
    u32 ptp_tx_ns;            /* 0x000001E4 (ro): PTP event frame transmitted nanoseconds, reset=0x00000000 */
    u32 ptp_rx_s;             /* 0x000001E8 (ro): PTP event frame received seconds, reset=0x00000000 */
    u32 ptp_rx_ns;            /* 0x000001EC (ro): PTP event frame received nanoseconds., reset=0x00000000 */
    u32 ptp_peer_tx_s;        /* 0x000001F0 (ro): PTP peer event frame transmitted seconds, reset=0x00000000 */
    u32 ptp_peer_tx_ns;       /* 0x000001F4 (ro): PTP peer event frame transmitted nanoseconds, reset=0x00000000 */
    u32 ptp_peer_rx_s;        /* 0x000001F8 (ro): PTP peer event frame received seconds, reset=0x00000000 */
    u32 ptp_peer_rx_ns;       /* 0x000001FC (ro): PTP peer event frame received nanoseconds., reset=0x00000000 */
    u32 reserved7[33];        /* 0x00000200 (no): reserved area */
    u32 design_cfg2;          /* 0x00000284 (ro): Design Configuration 2 */
    u32 design_cfg3;          /* 0x00000288 (ro): Design Configuration 3, reset=0x00000000 */
    u32 design_cfg4;          /* 0x0000028C (ro): Design Configuration 4, reset=0x00000000 */
    u32 design_cfg5;          /* 0x00000290 (ro): Design Configuration 5 */

} fnet_xemac_regs_t;

#endif /* _FNET_XC7Z_REGS_H_ */

