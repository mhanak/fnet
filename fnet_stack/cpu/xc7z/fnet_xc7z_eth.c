/**************************************************************************
*
* Copyright 2013 by Michal Hanak
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_xc7z_eth.c
*
* @author Michal Hanak
*
* @date Aug-2-2012
*
* @version 0.1.9.0
*
* @brief XC7Z Ethernet Interface.
*
***************************************************************************/

#include "fnet_config.h"
#include "fnet_netif.h"

#if FNET_XC7Z && (FNET_CFG_CPU_ETH0 || FNET_CFG_CPU_ETH1)

#include "fnet.h"
#include "fnet_xc7z_eth.h"

/************************************************************************
* Local types
*************************************************************************/

/* PHY clock divider search */
static const struct
{
    fnet_uint16 reg;
    fnet_uint16 divisor;
} fnet_xemac_mdc_div[] = {
    { MDC_DIV_8, 8 },
    { MDC_DIV_16, 16 },
    { MDC_DIV_32, 32 },
    { MDC_DIV_48, 48 },
    { MDC_DIV_64, 64 },
    { MDC_DIV_96, 96 },
    { MDC_DIV_128, 128 },
    { MDC_DIV_224, 224 },
    { 0, 0 }
};

/************************************************************************
* Local prototypes
*************************************************************************/

/* Ethernet PHY operations and PHY-related MAC setup */
static int fnet_eth_phy_init(fnet_xemac_if_t *ethif, int force_reset);
static int fnet_eth_phy_get_media_status(fnet_xemac_if_t* ethif, fnet_uint32* media_status);
static int fnet_eth_phy_update_mac_mode(fnet_xemac_if_t* ethif, fnet_uint32* mac_changed);
static void fnet_xemac_set_clock_and_duplex(fnet_xemac_if_t* ethif, fnet_uint32 media_status, fnet_uint32* mac_changed);

/* Ethernet Physical Transceiver control. */
static void fnet_xemac_phy_discover_addr(fnet_xemac_if_t *ethif, unsigned int phy_addr_start);
static int fnet_xemac_mii_write(fnet_xemac_if_t *ethif, int reg_addr, fnet_uint16 data);
static int fnet_xemac_mii_read(fnet_xemac_if_t *ethif, int reg_addr, fnet_uint16 *data);

/* interrupt handlers */
static void fnet_xemac_isr_rx_handler_top (void *cookie);
static void fnet_xemac_isr_rx_handler_bottom (void *cookie);

/* interrupt callbacks from XEmacPs low-level driver */
static void fnet_xemac_isr_callback_dmasend(void* param);
static void fnet_xemac_isr_callback_dmarecv(void* param);
static void fnet_xemac_isr_callback_error(void* param);

/* BD ring walk */
static void fnet_xemac_rx_buf_next( fnet_xemac_if_t * ethif);

/* low-level access */
static void fnet_xemac_start_low(fnet_xemac_if_t* ethif);
static fnet_uint32 fnet_xemac_getreg(fnet_xemac_if_t* ethif, fnet_uint32 byte_offset);

/************************************************************************
* Ethernet interface structure.
*************************************************************************/

/* XEMAC driver API */
extern const fnet_netif_api_t fnet_xemac_api;

/* SLCR access, and macro to create SLCR_GEM divisor register value */
#define SLCR_LOCK_ADDR                 (XPS_SYS_CTRL_BASEADDR + 0x4)
#define SLCR_UNLOCK_ADDR               (XPS_SYS_CTRL_BASEADDR + 0x8)
#define SLCR_LOCK_KEY_VALUE            0x767B
#define SLCR_UNLOCK_KEY_VALUE          0xDF0D
#define SLCR_GEM_DIV(reg, div0, div1)  ( (reg) | ((div1)<<20) | ((div0)<<8) )
#define SLCR_GEM_DIV_MASK              0x03F03F00
#define SLCR_GEM_10M_CLK_CTRL_VALUE    0x00100031
#define SLCR_GEM_100M_CLK_CTRL_VALUE   0x00100001
#define SLCR_GEM_1000M_CLK_CTRL_VALUE  0x00100011

#if FNET_CFG_CPU_ETH0

/* low driver-specific data structures - all Ethernet buffers and critical XEMAC data are here*/
fnet_xemac_if_t fnet_xemac0_if;
fnet_xemac_if_bufs_t FNET_DECLSPEC_ETH_BUF fnet_xemac0_bufs;

/* low driver-specific control data structure.*/
static const fnet_xemac_params_t fnet_xemac0_if_params =
{
    XPAR_PS7_TTC_0_TTC_CLK_FREQ_HZ,   /* clk_mdio; we don't have macro with ETH MDIO clock, but it is the same as for TTC */
    FNET_CFG_CPU_ETH0_PHY_ADDR,       /* phy_addr_init */
    FNET_CFG_CPU_ETH0_PHY_ADDR_DISCOVER, /* phy_auto_discover */
    FNET_CFG_CPU_ETH0_PHY_MDIO_CLK_KHZ,  /* phy_clock_khz */
    FNET_CFG_CPU_ETH0_AUTONEGOTIATION_ADVERTISE, /* autoneg_advertise */
    FNET_CFG_CPU_ETH0_AUTONEGOTIATION_TIMEOUT,   /* autoneg_timeout */
    FNET_CFG_CPU_ETH0_VECTOR_NUMBER,  /* vector_number; Vector number of the Ethernet Receive Frame interrupt.*/
    XPS_SYS_CTRL_BASEADDR + 0x140,    /* base address of SLCR_GEM_CLK_CTRL register */
    SLCR_GEM_DIV(SLCR_GEM_1000M_CLK_CTRL_VALUE, XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV0, XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV1), /* slcr_reg_1000 */
    SLCR_GEM_DIV(SLCR_GEM_100M_CLK_CTRL_VALUE,  XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV0,  XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV1),  /* slcr_reg_100 */
    SLCR_GEM_DIV(SLCR_GEM_10M_CLK_CTRL_VALUE,   XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV0,   XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV1),   /* slcr_reg_10 */
};

/* Ethernet-specific control data structure.*/
fnet_eth_if_t fnet_xc7z_eth0_if =
{
    &fnet_xemac0_if,               /* Low-level control data structure of the interface. */
    XPAR_PS7_ETHERNET_0_DEVICE_ID,
    fnet_xemac_output,
#if FNET_CFG_MULTICAST
    fnet_xemac_multicast_join,
    fnet_xemac_multicast_leave,
#endif /* FNET_CFG_MULTICAST */     
};

/* Network interface.*/
fnet_netif_t fnet_eth0_if =
{
    0,                          /* Pointer to the next net_if structure.*/
    0,                          /* Pointer to the previous net_if structure.*/
    "eth0",                     /* Network interface name.*/
    FNET_CFG_CPU_ETH0_MTU,      /* Maximum transmission unit.*/
    &fnet_xc7z_eth0_if,         /* Points to interface specific data structure.*/
    &fnet_xemac_api             /* Interface API */
};
#endif

#if FNET_CFG_CPU_ETH1

/* low driver-specific data structures - all Ethernet buffers and critical XEMAC data are here*/
fnet_xemac_if_t fnet_xemac1_if;
fnet_xemac_if_bufs_t FNET_DECLSPEC_ETH_BUF fnet_xemac1_bufs;

/* low driver-specific control data structure.*/
static const fnet_xemac_params_t fnet_xemac1_if_params =
{
    XPAR_PS7_TTC_0_TTC_CLK_FREQ_HZ,   /* clk_mdio; we don't have macro with ETH MDIO clock, but it is the same as for TTC */
    FNET_CFG_CPU_ETH1_PHY_ADDR,       /* phy_addr_init */
    FNET_CFG_CPU_ETH1_PHY_ADDR_DISCOVER, /* phy_auto_discover */
    FNET_CFG_CPU_ETH1_PHY_MDIO_CLK_KHZ,  /* phy_clock_khz */
    FNET_CFG_CPU_ETH1_AUTONEGOTIATION_ADVERTISE, /* autoneg_advertise */
    FNET_CFG_CPU_ETH1_AUTONEGOTIATION_TIMEOUT,   /* autoneg_timeout */
    FNET_CFG_CPU_ETH1_VECTOR_NUMBER,  /* vector_number; Vector number of the Ethernet Receive Frame interrupt.*/
    XPS_SYS_CTRL_BASEADDR + 0x144,    /* base address of SLCR_GEM_CLK_CTRL register */
    SLCR_GEM_DIV(SLCR_GEM_1000M_CLK_CTRL_VALUE, XPAR_PS7_ETHERNET_1_ENET_SLCR_1000MBPS_DIV0, XPAR_PS7_ETHERNET_1_ENET_SLCR_1000MBPS_DIV1), /* slcr_reg_1000 */
    SLCR_GEM_DIV(SLCR_GEM_100M_CLK_CTRL_VALUE,  XPAR_PS7_ETHERNET_1_ENET_SLCR_100MBPS_DIV0,  XPAR_PS7_ETHERNET_1_ENET_SLCR_100MBPS_DIV1),  /* slcr_reg_100 */
    SLCR_GEM_DIV(SLCR_GEM_10M_CLK_CTRL_VALUE,   XPAR_PS7_ETHERNET_1_ENET_SLCR_10MBPS_DIV0,   XPAR_PS7_ETHERNET_1_ENET_SLCR_10MBPS_DIV1),   /* slcr_reg_10 */
};

/* Ethernet-specific control data structure.*/
fnet_eth_if_t fnet_xc7z_eth1_if =
{
    &fnet_xemac1_if,               /* Low-level control data structure of the interface. */
    XPAR_PS7_ETHERNET_1_DEVICE_ID,
    fnet_xemac_output,
#if FNET_CFG_MULTICAST
    fnet_xemac_multicast_join,
    fnet_xemac_multicast_leave,
#endif /* FNET_CFG_MULTICAST */
};

/* Network interface.*/
fnet_netif_t fnet_eth1_if =
{
    0,                          /* Pointer to the next net_if structure.*/
    0,                          /* Pointer to the previous net_if structure.*/
    "eth1",                     /* Network interface name.*/
    FNET_CFG_CPU_ETH1_MTU,      /* Maximum transmission unit.*/
    &fnet_xc7z_eth1_if,         /* Points to interface specific data structure.*/
    &fnet_xemac_api             /* Interface API */
};
#endif

/*****************************************************************************
 * XEMAC general API structure.
 ******************************************************************************/

const fnet_netif_api_t fnet_xemac_api =
{
    FNET_NETIF_TYPE_ETHERNET,       /* Data-link type. */
    sizeof(fnet_mac_addr_t),
    fnet_xemac_init,                  /* Initialization function.*/
    fnet_xemac_release,               /* Shutdown function.*/
#if FNET_CFG_IP4
    fnet_eth_output_ip4,             /* IPv4 Transmit function.*/
#endif
    fnet_eth_change_addr_notify,     /* Address change notification function.*/
    fnet_eth_drain,                  /* Drain function.*/
    fnet_xemac_get_hw_addr,
    fnet_xemac_set_hw_addr,
    fnet_xemac_is_connected,
    fnet_xemac_get_statistics
#if FNET_CFG_MULTICAST
    #if FNET_CFG_IP4
        ,fnet_eth_multicast_join_ip4
        ,fnet_eth_multicast_leave_ip4
    #endif
    #if FNET_CFG_IP6
        ,fnet_eth_multicast_join_ip6
        ,fnet_eth_multicast_leave_ip6
    #endif
#endif
#if FNET_CFG_IP6
    ,fnet_eth_output_ip6            /* IPv6 Transmit function.*/
#endif
};


/************************************************************************
* NAME: fnet_xemac_init
*
* DESCRIPTION: Ethernet module initialization.
*************************************************************************/
int fnet_xemac_init(fnet_netif_t *netif)
{
    XEmacPs_Config* pcfg;
    int i, result = FNET_OK;
    fnet_eth_if_t* ifptr;
    fnet_xemac_if_t* ethif;
    fnet_uint32 clk, div;

    ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);
    FNET_ASSERT(ethif != NULL);

    /* this is set true later only if everything goes well */
    ethif->status_flags = 0;

    /* initalize the low-level driver */
    pcfg = XEmacPs_LookupConfig(ifptr->mac_number);
    if(pcfg)
    {
        XEmacPs_CfgInitialize(&ethif->xemacps_low, pcfg, pcfg->BaseAddress);
        XEmacPs_Reset(&ethif->xemacps_low);
        XEmacPs_WriteReg(ethif->xemacps_low.Config.BaseAddress, XEMACPS_TXSR_OFFSET, 0xffffffff);
        XEmacPs_WriteReg(ethif->xemacps_low.Config.BaseAddress, XEMACPS_RXSR_OFFSET, 0xffffffff);

        /* by default, use the same MAC instance for MDIO communication */
        ethif->xemacps_low_mii = &ethif->xemacps_low;

        if(0) ;
#if FNET_CFG_CPU_ETH0
        else if(ifptr->mac_number == 0)
        {
            ethif->pparams = &fnet_xemac0_if_params;
            ethif->bufs = &fnet_xemac0_bufs;
        }
#endif
#if FNET_CFG_CPU_ETH1
        else if(ifptr->mac_number == 1)
        {
            ethif->pparams = &fnet_xemac1_if_params;
            ethif->bufs = &fnet_xemac1_bufs;
#if FNET_CFG_CPU_ETH1_PHY_SHARED_MDIO
            ethif->xemacps_low_mii = fnet_xemac0_if.xemacps_low_mii;
#endif
        }
#endif
        else
        {
            result = FNET_ERR;
        }
    }
    else
    {
        result = FNET_ERR;
    }

    /* ======== Ethernet buffers initialisation ===============*/
    if(result == FNET_OK)
    {
        FNET_ASSERT(ethif->bufs != NULL);

        /* initialize bd rings */
        ethif->tx_buf_desc = (fnet_xemac_bd_t*) FNET_XEMAC_ALIGN_UP(FNET_XEMAC_BDRING_ALIGNMENT, ethif->bufs->tx_bdring_mem);
        ethif->rx_buf_desc = (fnet_xemac_bd_t*) FNET_XEMAC_ALIGN_UP(FNET_XEMAC_BDRING_ALIGNMENT, ethif->bufs->rx_bdring_mem);

        ethif->tx_buf_desc_cur = ethif->tx_buf_desc;
        ethif->rx_buf_desc_cur = ethif->rx_buf_desc;

        /* register rings with the driver so the base address gets used properly */
        /* this is the only place where Xilinx BDRING driver gets used */
        XEmacPs_BdRingCreate(&XEmacPs_GetTxRing(&ethif->xemacps_low), (u32)ethif->tx_buf_desc, (u32)ethif->tx_buf_desc,
                FNET_XEMAC_BD_ALIGNMENT, FNET_XEMAC_TX_BUF_NUM);
        XEmacPs_BdRingCreate(&XEmacPs_GetRxRing(&ethif->xemacps_low), (u32)ethif->rx_buf_desc, (u32)ethif->rx_buf_desc,
                FNET_XEMAC_BD_ALIGNMENT, FNET_XEMAC_RX_BUF_NUM);

        /* Initialize TX descriptors */
        for (i = 0; i < FNET_XEMAC_TX_BUF_NUM; i++)
        {
            ethif->tx_buf_desc[i].stat = XEMACPS_TXBUF_LAST_MASK | XEMACPS_TXBUF_USED_MASK;
            ethif->tx_buf_desc[i].addr = (fnet_uint32) FNET_XEMAC_ALIGN_UP(FNET_XEMAC_TXBUF_ALIGNMENT, ethif->bufs->tx_bufs_mem[i]);
        }
        ethif->tx_buf_desc_num = FNET_XEMAC_TX_BUF_NUM;

        /* Initialize Rx descriptor rings.*/
        for (i = 0; i < FNET_XEMAC_RX_BUF_NUM; i++)
        {
            ethif->rx_buf_desc[i].stat = (FNET_XEMAC_BUF_SIZE & XEMACPS_RXBUF_LEN_MASK);
            ethif->rx_buf_desc[i].addr = (fnet_uint32) FNET_XEMAC_ALIGN_UP(FNET_XEMAC_TXBUF_ALIGNMENT, ethif->bufs->rx_bufs_mem[i]);
        }
        ethif->rx_buf_desc_num = FNET_XEMAC_RX_BUF_NUM;

        /* Set the Wrap bit on the last one in the ring */
        ethif->tx_buf_desc[FNET_XEMAC_TX_BUF_NUM-1].stat |= XEMACPS_TXBUF_WRAP_MASK;
        ethif->rx_buf_desc[FNET_XEMAC_RX_BUF_NUM-1].addr |= XEMACPS_RXBUF_WRAP_MASK;  /* WRAP bit in RX BD is in ADDR field */

        /*======== END of Ethernet buffers initialization ========*/

        /* Set interrupt handler callbacks (must be done even if we do not really use them) */
        XEmacPs_SetHandler(&ethif->xemacps_low, XEMACPS_HANDLER_DMASEND, fnet_xemac_isr_callback_dmasend, (void*)netif);
        XEmacPs_SetHandler(&ethif->xemacps_low, XEMACPS_HANDLER_DMARECV, fnet_xemac_isr_callback_dmarecv, (void*)netif);
        XEmacPs_SetHandler(&ethif->xemacps_low, XEMACPS_HANDLER_ERROR, fnet_xemac_isr_callback_error, (void*)netif);

        /* Install RX Frame interrupt handler.*/
        result = fnet_isr_vector_init(ethif->pparams->vector_number, fnet_xemac_isr_rx_handler_top,
                fnet_xemac_isr_rx_handler_bottom, FNET_CFG_CPU_ETH_VECTOR_PRIORITY, (void *)netif);
    }

    /* XEMAC and PHY options */
    if( result == FNET_OK)
    {
        /* clear multicast hash */
        XEmacPs_ClearHash(&ethif->xemacps_low);

        /* default operating speed */
        fnet_xemac_set_clock_and_duplex(ethif, FNET_ETH_PHY_MEDIA_100FULL, NULL);

        /* default options */
        XEmacPs_SetOptions(&ethif->xemacps_low,
                //XEMACPS_FLOW_CONTROL_OPTION |
                XEMACPS_FCS_INSERT_OPTION |
                XEMACPS_FCS_STRIP_OPTION |
                XEMACPS_BROADCAST_OPTION |
                XEMACPS_LENTYPE_ERR_OPTION |
#if FNET_CFG_CPU_ETH_HW_RX_IP_CHECKSUM || FNET_CFG_CPU_ETH_HW_RX_PROTOCOL_CHECKSUM
                XEMACPS_RX_CHKSUM_ENABLE_OPTION |
#endif
#if FNET_CFG_CPU_ETH_HW_TX_IP_CHECKSUM || FNET_CFG_CPU_ETH_HW_TX_PROTOCOL_CHECKSUM
                XEMACPS_TX_CHKSUM_ENABLE_OPTION |
#endif
#if FNET_CFG_CPU_ETH_PROMISCUOUS
                XEMACPS_PROMISC_OPTION
#endif
#if FNET_CFG_MULTICAST
                XEMACPS_MULTICAST_OPTION |
#endif
                /* XEMACPS_RECEIVER_ENABLE_OPTION | */     /* THIS is causing HRESP error (probably now it is too early?)  */
                /* XEMACPS_TRANSMITTER_ENABLE_OPTION | */  /* as they are default options, both RX and TX are enabled upon start */
                0);

        /* Set network interface features, for upper layer.*/
        netif->features = 0
#if FNET_CFG_CPU_ETH_HW_RX_IP_CHECKSUM || FNET_CFG_CPU_ETH_HW_RX_PROTOCOL_CHECKSUM
                | FNET_NETIF_FEATURE_HW_RX_IP_CHECKSUM          /* RX IP header checksum feature.*/
                | FNET_NETIF_FEATURE_HW_RX_PROTOCOL_CHECKSUM    /* RX Protocol checksum feature.*/
#endif
#if FNET_CFG_CPU_ETH_HW_TX_IP_CHECKSUM || FNET_CFG_CPU_ETH_HW_TX_PROTOCOL_CHECKSUM
                | FNET_NETIF_FEATURE_HW_TX_IP_CHECKSUM          /* TX IP header checksum feature.*/
                | FNET_NETIF_FEATURE_HW_TX_PROTOCOL_CHECKSUM    /* RX Protocol checksum feature.*/
#endif
                    ;

        /* find best divisor to achieve required MDIO clock */
        for(i=0, clk=0; fnet_xemac_mdc_div[i].divisor > 0; i++)
        {
            div = fnet_xemac_mdc_div[i].divisor * 1000;    /* use kHz */
            clk = ethif->pparams->clk_mdio / div;
            if(clk <= ethif->pparams->phy_clock_khz)
                break;
        }

        if(fnet_xemac_mdc_div[i].divisor > 0)
        {
            XEmacPs_SetMdioDivisor(&ethif->xemacps_low, fnet_xemac_mdc_div[i].reg);
            ethif->mdio_clk_khz = clk;
        }
        else
        {
            /* divisor not found */
            result = FNET_ERR;
            ethif->mdio_clk_khz = 0;
        }
    }

    /* PHY initialization */
    if(result == FNET_OK)
    {
        if(ethif->pparams->phy_auto_discover)
            fnet_xemac_phy_discover_addr(ethif, ethif->pparams->phy_addr_init);
        /* initialize PHY and auto-negotiation */
        fnet_eth_phy_init(ethif, 0);
    }

    if(result == FNET_OK)
    {
        /* remember that we want to start as soon as possible
         * (as soon as MAC will be assigned) */
        ethif->status_flags = FNET_XEMACIFF_IS_CONFIGURED | FNET_XEMACIFF_IS_RUN_REQUEST;
    }

    return result;
}

/************************************************************************
* NAME: fnet_xemac_start_low
*
* DESCRIPTION: Helper function to start the interface.
*************************************************************************/
static void fnet_xemac_start_low(fnet_xemac_if_t* ethif)
{
    int i;

    FNET_ASSERT(ethif != NULL);
    FNET_ASSERT(ethif->status_flags & FNET_XEMACIFF_IS_CONFIGURED);
    FNET_ASSERT(ethif->status_flags & FNET_XEMACIFF_IS_RUN_REQUEST);

    /* starting low-level driver resets pointers to BD rings, we need to empty them all */
    ethif->tx_buf_desc_cur = ethif->tx_buf_desc;
    ethif->rx_buf_desc_cur = ethif->rx_buf_desc;

    for (i = 0; i < FNET_XEMAC_TX_BUF_NUM; i++)
        ethif->tx_buf_desc[i].stat |= XEMACPS_TXBUF_USED_MASK;
    for (i = 0; i < FNET_XEMAC_RX_BUF_NUM; i++)
        ethif->rx_buf_desc[i].addr &= ~XEMACPS_RXBUF_NEW_MASK;

    /* Set the Wrap bit on the last ones in the ring */
    ethif->tx_buf_desc[FNET_XEMAC_TX_BUF_NUM-1].stat |= XEMACPS_TXBUF_WRAP_MASK;
    ethif->rx_buf_desc[FNET_XEMAC_RX_BUF_NUM-1].addr |= XEMACPS_RXBUF_WRAP_MASK;

    /* start normally (this enables all interrupts) */
    XEmacPs_Start(&ethif->xemacps_low);

    /* we do not need all interrupts enabled, only care about RX */
    XEmacPs_IntDisable(&ethif->xemacps_low, (XEMACPS_IXR_ALL_MASK));
    XEmacPs_IntEnable(&ethif->xemacps_low, (XEMACPS_IXR_RX_ERR_MASK | XEMACPS_IXR_FRAMERX_MASK));
}

/************************************************************************
* NAME: fnet_xemac_release
*
* DESCRIPTION: Ethernet module release.
*************************************************************************/
void fnet_xemac_release(fnet_netif_t *netif)
{
    fnet_eth_if_t* ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    fnet_xemac_if_t* ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);

    FNET_ASSERT(ethif != NULL);

    ethif->status_flags = 0;
    XEmacPs_Reset(&ethif->xemacps_low);
}

/************************************************************************
* NAME: fnet_xemac_stop
*
* DESCRIPTION: Stop Ethernet module.
*              For debugging needs.
*************************************************************************/
void fnet_xemac_stop(fnet_netif_t *netif)
{
    fnet_eth_if_t* ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    fnet_xemac_if_t* ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);

    FNET_ASSERT(ethif != NULL);

    ethif->status_flags &= ~FNET_XEMACIFF_IS_RUN_REQUEST;
    XEmacPs_Stop(&ethif->xemacps_low);
}

/************************************************************************
* NAME: fnet_xemac_resume
*
* DESCRIPTION: Resume Ethernet module.
*              For debugging needs.
*************************************************************************/
void fnet_xemac_resume(fnet_netif_t *netif)
{
    fnet_eth_if_t* ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    fnet_xemac_if_t* ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);

    FNET_ASSERT(ethif != NULL);

    if(ethif->status_flags & FNET_XEMACIFF_IS_CONFIGURED)
    {
       ethif->status_flags |= FNET_XEMACIFF_IS_RUN_REQUEST;
       fnet_xemac_start_low(ethif);
    }
}

/************************************************************************
* NAME: fnet_xemac_input
*
* DESCRIPTION: Ethernet input function.
*************************************************************************/
void fnet_xemac_input(fnet_netif_t *netif)
{
    fnet_eth_if_t* ifptr;
    fnet_xemac_if_t* ethif;
    fnet_eth_header_t * ethheader;
    fnet_netbuf_t * nb = NULL;

    FNET_ASSERT(netif != NULL);
    ifptr = (fnet_eth_if_t *)(netif->if_ptr);

    FNET_ASSERT(ifptr != NULL);
    ethif = ifptr->if_cpu_ptr;

    FNET_ASSERT(ethif != NULL);
    FNET_ASSERT(ethif->bufs != NULL);

    /* While there is a valid buffer (buffer with received data) */
    while(ethif->rx_buf_desc_cur->addr & XEMACPS_RXBUF_NEW_MASK)
    {

#if !FNET_CFG_CPU_ETH_MIB
        ifptr->statistics.rx_packet++;
#endif

        /* If buffer holds incomplete packet) */
        if ((ethif->rx_buf_desc_cur->stat & (XEMACPS_RXBUF_EOF_MASK|XEMACPS_RXBUF_SOF_MASK)) != (XEMACPS_RXBUF_EOF_MASK|XEMACPS_RXBUF_SOF_MASK))
        {
            /* Skip all such split buffers. */
            do
            {
                fnet_xemac_rx_buf_next(ethif);
            }
            while ((ethif->rx_buf_desc_cur->addr & XEMACPS_RXBUF_NEW_MASK) && /* Keep going until we find the last one. */
                   (ethif->rx_buf_desc_cur->stat & (XEMACPS_RXBUF_EOF_MASK|XEMACPS_RXBUF_SOF_MASK)) != (XEMACPS_RXBUF_EOF_MASK|XEMACPS_RXBUF_SOF_MASK) );
        }
        else
        {
            /* Point to the Ethernet header.*/
            ethheader = (fnet_eth_header_t *)(ethif->rx_buf_desc_cur->addr & XEMACPS_RXBUF_ADD_MASK);

            /* Ignore self-received packets.*/
            if(!fnet_memcmp(ethheader->source_addr, ethif->local_mac_addr, sizeof(fnet_mac_addr_t)))
            {
                goto NEXT_FRAME;
            }


            fnet_eth_trace("\nRX", ethheader); /* Print ETH header.*/

            nb = fnet_netbuf_from_buf( (void *)((unsigned long)ethheader + sizeof(fnet_eth_header_t)),
                                        (int)(ethif->rx_buf_desc_cur->stat & XEMACPS_RXBUF_LEN_MASK) - sizeof(fnet_eth_header_t),
                                        FNET_TRUE);
            if(nb)
            {
                if(ethif->rx_buf_desc_cur->stat & XEMACPS_RXBUF_BCAST_MASK)    /* Broadcast */
                {
                    nb->flags |= FNET_NETBUF_FLAG_BROADCAST;
                }

                if (ethif->rx_buf_desc_cur->stat & XEMACPS_RXBUF_MULTIHASH_MASK) /* Multicast */
                {
                    nb->flags |= FNET_NETBUF_FLAG_MULTICAST;
                }

                /* Network-layer input.*/
                fnet_eth_prot_input( netif, nb, ethheader->type );
            }
         }
NEXT_FRAME:
         fnet_xemac_rx_buf_next(ethif);
    }; /* while */
}

/************************************************************************
* NAME: fnet_xemac_rx_buf_next
*
* DESCRIPTION: Goes not the next Rx buffer.
*************************************************************************/
static void fnet_xemac_rx_buf_next(fnet_xemac_if_t * ethif)
{
   /* Mark the buffer as empty.*/
   ethif->rx_buf_desc_cur->addr &= ~XEMACPS_RXBUF_NEW_MASK;

   /* Update pointer to next entry.*/
   if (ethif->rx_buf_desc_cur->addr & XEMACPS_RXBUF_WRAP_MASK)
      ethif->rx_buf_desc_cur = ethif->rx_buf_desc;
   else
      ethif->rx_buf_desc_cur++;
}

/************************************************************************
* NAME: fnet_xemac_output
*
* DESCRIPTION: Ethernet low-level output function.
*************************************************************************/
fnet_vuint32 fnet_xemac_output_waitcount = 0;

void fnet_xemac_output(fnet_netif_t *netif, unsigned short type, const fnet_mac_addr_t dest_addr, fnet_netbuf_t* nb)
{
    fnet_eth_if_t* ifptr;
    fnet_xemac_if_t* ethif;
    fnet_eth_header_t * ethheader;
    fnet_uint32 wrap;

    FNET_ASSERT(netif != NULL);
    ifptr = (fnet_eth_if_t *)(netif->if_ptr);

    FNET_ASSERT(ifptr != NULL);
    ethif = ifptr->if_cpu_ptr;
    FNET_ASSERT(ethif != NULL);

    /* this function may be called from main as well as from FNET ISR (not from user ISRs!) */
    fnet_isr_lock();

    if(nb && nb->total_length<=netif->mtu)
    {
        /* wait for TX buffer to be free */
        while((ethif->tx_buf_desc_cur->stat & XEMACPS_TXBUF_USED_MASK) == 0)
        {
            /* if you hang here for long time, some race condition occurred (send called from user ISR?) */
            fnet_xemac_output_waitcount++;

            /* debugging the TX buffers and GEM registers to find out what is wrong
            if(waitcount == 0x100000)
            {
                int ii;
                for(ii=0; ii<ethif->tx_buf_desc_num; ii++)
                    fnet_shell_printf(desc, "buf%i addr=0x%08x stat=0x%08x\n", ii, ethif->tx_buf_desc[ii].addr, ethif->tx_buf_desc[ii].stat);

                    fnet_eth_debug_mii_print_regs(netif);
            }
            */
        };

        /* pointer to the frame memory */
        ethheader = (fnet_eth_header_t *) ethif->tx_buf_desc_cur->addr;

        /* copy frame data */
        fnet_netbuf_to_buf(nb, 0, FNET_NETBUF_COPYALL, (void *)((unsigned long)ethheader + FNET_ETH_HDR_SIZE));

        /* set destination address */
        fnet_memcpy(ethheader->destination_addr, dest_addr, sizeof(fnet_mac_addr_t));

        /* set source address (=our MAC address) */
        fnet_memcpy(ethheader->source_addr, ethif->local_mac_addr, sizeof(fnet_mac_addr_t));

        /* set type */
        ethheader->type = fnet_htons(type);

        /* set length and mark frame ready for transmitting (USED bit gets cleared) */
        wrap = ethif->tx_buf_desc_cur->stat & XEMACPS_TXBUF_WRAP_MASK;  /* WRAP bit needs to remain unchanged */
        ethif->tx_buf_desc_cur->stat = wrap | XEMACPS_TXBUF_LAST_MASK | ((FNET_ETH_HDR_SIZE + nb->total_length) & XEMACPS_TXBUF_LEN_MASK);

        /* trigger transmission */
        XEmacPs_Transmit((&ethif->xemacps_low));

        /* Update pointer to next entry.*/
        if (wrap)
            ethif->tx_buf_desc_cur = ethif->tx_buf_desc;
        else
            ethif->tx_buf_desc_cur++;

#if !FNET_CFG_CPU_ETH_MIB
        ifptr->statistics.tx_packet++;
#endif
    }

    fnet_netbuf_free_chain(nb);

    fnet_isr_unlock();
}

/************************************************************************
* NAME: fnet_xemac_set_hw_addr
*
* DESCRIPTION: This function sets MAC address.
*************************************************************************/
int fnet_xemac_set_hw_addr(fnet_netif_t *netif, unsigned char * hw_addr)
{
    fnet_eth_if_t* ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    fnet_xemac_if_t* ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);
    int xres, result;

    FNET_ASSERT(ethif != NULL);

    /* Set only non-zero MAC address. User may set the initial MAC address to 0 in
     * the fnet_user_config.h file in order to postpone the automatic interface
     * startup during fnet_netif_init(). */
    if((hw_addr[0] | hw_addr[1] | hw_addr[2] | hw_addr[3] | hw_addr[4] | hw_addr[5]) == 0)
        return FNET_ERR;

    /* XC7 requires to be stopped before changing MAC address */
    if(ethif->xemacps_low.IsStarted == XIL_COMPONENT_IS_STARTED)
        XEmacPs_Stop(&ethif->xemacps_low);

    /* set the new MAC address */
    xres = XEmacPs_SetMacAddress(&ethif->xemacps_low, hw_addr, 1);
    result = xres == XST_SUCCESS ? FNET_OK : FNET_ERR;

    if(result == FNET_OK)
    {
        /* cache the address also locally for later use */
        fnet_memcpy(ethif->local_mac_addr, hw_addr, sizeof(fnet_mac_addr_t));

        /* if we should be running, re-start the MAC again. Note that this is also where the
         * MAC is started for the very first time after initialization, once the MAC address
         * is specified. */
        if(ethif->status_flags & FNET_XEMACIFF_IS_RUN_REQUEST)
            fnet_xemac_start_low(ethif);
    }

    return result;
}

/************************************************************************
* NAME: fnet_xemac_get_hw_addr
*
* DESCRIPTION: This function reads HW address.
*************************************************************************/
int fnet_xemac_get_hw_addr(fnet_netif_t *netif, unsigned char * hw_addr)
{
    fnet_eth_if_t* ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    fnet_xemac_if_t* ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);
    int result;

    if(ethif && hw_addr)
    {
        fnet_memcpy(hw_addr, ethif->local_mac_addr, sizeof(fnet_mac_addr_t));
        result = FNET_OK;
    }
    else
    {
        result = FNET_ERR;
    }

    return result;
}

/************************************************************************
* NAME: fnet_xemac_get_statistics
*
* DESCRIPTION: Returns Ethernet statistics information
*************************************************************************/
int fnet_xemac_get_statistics(struct fnet_netif *netif, struct fnet_netif_statistics * statistics)
{
    fnet_eth_if_t* ifptr = (fnet_eth_if_t *)(netif ? netif->if_ptr : NULL);
    fnet_xemac_if_t* ethif = (fnet_xemac_if_t*)(ifptr ? ifptr->if_cpu_ptr : NULL);
    fnet_uint32* pdelta;
    fnet_uint32* ptotal;
    int i, count;

    if(!ethif)
        return FNET_ERR;

    /* our HW MIB counters get auto-cleared when read, get the delta since last called */
    ethif->mib_delta.tx_all            = fnet_xemac_getreg(ethif, XEMACPS_TXCNT_OFFSET);
    ethif->mib_delta.tx_bcast          = fnet_xemac_getreg(ethif, XEMACPS_TXBCCNT_OFFSET);
    ethif->mib_delta.tx_mcast          = fnet_xemac_getreg(ethif, XEMACPS_TXMCCNT_OFFSET);
    ethif->mib_delta.tx_pause          = fnet_xemac_getreg(ethif, XEMACPS_TXPAUSECNT_OFFSET);
    ethif->mib_delta.tx_64             = fnet_xemac_getreg(ethif, XEMACPS_TX64CNT_OFFSET);
    ethif->mib_delta.tx_64_127         = fnet_xemac_getreg(ethif, XEMACPS_TX65CNT_OFFSET);
    ethif->mib_delta.tx_128_255        = fnet_xemac_getreg(ethif, XEMACPS_TX128CNT_OFFSET);
    ethif->mib_delta.tx_256_511        = fnet_xemac_getreg(ethif, XEMACPS_TX256CNT_OFFSET);
    ethif->mib_delta.tx_512_1023       = fnet_xemac_getreg(ethif, XEMACPS_TX512CNT_OFFSET);
    ethif->mib_delta.tx_1024_1518      = fnet_xemac_getreg(ethif, XEMACPS_TX1024CNT_OFFSET);
    ethif->mib_delta.tx_over_1519      = fnet_xemac_getreg(ethif, XEMACPS_TX1519CNT_OFFSET);
    ethif->mib_delta.tx_err_underrun   = fnet_xemac_getreg(ethif, XEMACPS_TXURUNCNT_OFFSET);
    ethif->mib_delta.tx_err_single_col = fnet_xemac_getreg(ethif, XEMACPS_SNGLCOLLCNT_OFFSET);
    ethif->mib_delta.tx_err_multi_col  = fnet_xemac_getreg(ethif, XEMACPS_MULTICOLLCNT_OFFSET);
    ethif->mib_delta.tx_err_excess_col = fnet_xemac_getreg(ethif, XEMACPS_EXCESSCOLLCNT_OFFSET);
    ethif->mib_delta.tx_err_late_col   = fnet_xemac_getreg(ethif, XEMACPS_LATECOLLCNT_OFFSET);
    ethif->mib_delta.tx_err_deferred   = fnet_xemac_getreg(ethif, XEMACPS_TXDEFERCNT_OFFSET);
    ethif->mib_delta.tx_err_cs         = fnet_xemac_getreg(ethif, XEMACPS_TXCSENSECNT_OFFSET);

    ethif->mib_delta.rx_all            = fnet_xemac_getreg(ethif, XEMACPS_RXCNT_OFFSET);
    ethif->mib_delta.rx_bcast          = fnet_xemac_getreg(ethif, XEMACPS_RXBROADCNT_OFFSET);
    ethif->mib_delta.rx_mcast          = fnet_xemac_getreg(ethif, XEMACPS_RXMULTICNT_OFFSET);
    ethif->mib_delta.rx_pause          = fnet_xemac_getreg(ethif, XEMACPS_RXPAUSECNT_OFFSET);
    ethif->mib_delta.rx_64             = fnet_xemac_getreg(ethif, XEMACPS_RX64CNT_OFFSET);
    ethif->mib_delta.rx_64_127         = fnet_xemac_getreg(ethif, XEMACPS_RX65CNT_OFFSET);
    ethif->mib_delta.rx_128_255        = fnet_xemac_getreg(ethif, XEMACPS_RX128CNT_OFFSET);
    ethif->mib_delta.rx_256_511        = fnet_xemac_getreg(ethif, XEMACPS_RX256CNT_OFFSET);
    ethif->mib_delta.rx_512_1023       = fnet_xemac_getreg(ethif, XEMACPS_RX512CNT_OFFSET);
    ethif->mib_delta.rx_1024_1518      = fnet_xemac_getreg(ethif, XEMACPS_RX1024CNT_OFFSET);
    ethif->mib_delta.rx_over_1519      = fnet_xemac_getreg(ethif, XEMACPS_RX1519CNT_OFFSET);
    ethif->mib_delta.rx_err_undersize  = fnet_xemac_getreg(ethif, XEMACPS_RXUNDRCNT_OFFSET);
    ethif->mib_delta.rx_err_oversize   = fnet_xemac_getreg(ethif, XEMACPS_RXOVRCNT_OFFSET);
    ethif->mib_delta.rx_err_jabbers    = fnet_xemac_getreg(ethif, XEMACPS_RXJABCNT_OFFSET);
    ethif->mib_delta.rx_err_fcs        = fnet_xemac_getreg(ethif, XEMACPS_RXFCSCNT_OFFSET);
    ethif->mib_delta.rx_err_len        = fnet_xemac_getreg(ethif, XEMACPS_RXLENGTHCNT_OFFSET);
    ethif->mib_delta.rx_err_symb       = fnet_xemac_getreg(ethif, XEMACPS_RXSYMBCNT_OFFSET);
    ethif->mib_delta.rx_err_align      = fnet_xemac_getreg(ethif, XEMACPS_RXALIGNCNT_OFFSET);
    ethif->mib_delta.rx_err_res        = fnet_xemac_getreg(ethif, XEMACPS_RXRESERRCNT_OFFSET);
    ethif->mib_delta.rx_err_ovr        = fnet_xemac_getreg(ethif, XEMACPS_RXORCNT_OFFSET);
    ethif->mib_delta.rx_err_ipcs       = fnet_xemac_getreg(ethif, XEMACPS_RXIPCCNT_OFFSET);
    ethif->mib_delta.rx_err_tcpcs      = fnet_xemac_getreg(ethif, XEMACPS_RXTCPCCNT_OFFSET);
    ethif->mib_delta.rx_err_udpcs      = fnet_xemac_getreg(ethif, XEMACPS_RXUDPCCNT_OFFSET);

    /* add deltas to totals */
    count = sizeof(fnet_xemac_statistics_t)/sizeof(fnet_uint32);
    pdelta = (fnet_uint32*)&ethif->mib_delta;
    ptotal = (fnet_uint32*)&ethif->mib_total;
    for(i=0; i<count; i++, ptotal++, pdelta++)
        *ptotal += *pdelta;

    /* get the data to the caller's format */
    if(statistics)
    {
        statistics->tx_packet = ethif->mib_total.tx_all;
        statistics->rx_packet = ethif->mib_total.rx_all;
    }

    return FNET_OK;
}

/************************************************************************
* NAME: fnet_xemac_mii_read
*
* DESCRIPTION: Read a value from a PHY's MII register.
* reg_addr < address of the register in the PHY
* data < Pointer to storage for the Data to be read from the PHY register (passed by reference)
* Return FNET_ERR on failure, FNET_OK on success
*************************************************************************/
static int fnet_xemac_mii_read(fnet_xemac_if_t *ethif, int reg_addr, fnet_uint16 *data)
{
    int xresult;

    FNET_ASSERT(ethif != NULL);

    xresult = XEmacPs_PhyRead(ethif->xemacps_low_mii, ethif->phy_addr, reg_addr, data);
    return xresult == XST_SUCCESS ? FNET_OK : FNET_ERR;
}

/************************************************************************
* NAME: fnet_xemac_mii_write
*
* DESCRIPTION: Write a value to a PHY's MII register.
* reg_addr < address of the register in the PHY
* data < Data to be writen to the PHY register (passed by reference)
* Return 0 on failure (timeout), 1 on success
*************************************************************************/
int fnet_xemac_mii_write(fnet_xemac_if_t *ethif, int reg_addr, fnet_uint16 data)
{
    int xresult;

    FNET_ASSERT(ethif != NULL);

    xresult = XEmacPs_PhyWrite(ethif->xemacps_low_mii, ethif->phy_addr, reg_addr, data);
    return xresult == XST_SUCCESS ? FNET_OK : FNET_ERR;
}

/************************************************************************
* NAME: fnet_xemac_phy_discover_addr
*
* DESCRIPTION: Looking for a valid PHY address.
*************************************************************************/
static void fnet_xemac_phy_discover_addr(fnet_xemac_if_t *ethif, unsigned int phy_addr_start)
{
    fnet_uint16 id1, id2;
    unsigned int i;

    for (i = phy_addr_start; i < 32; i++)
    {

        ethif->phy_addr = i;
        fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_IDR1, &id1);
        fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_IDR2, &id2);

        if (!(id1 == 0 || id1 == 0xffff || id1 == 0x7fff))
        {
            /* seems like valid PHY */
            break;
        }
    }
}

/************************************************************************
* NAME: fnet_xemac_is_connected
*
* DESCRIPTION: Link status.
*************************************************************************/
int fnet_xemac_is_connected(fnet_netif_t *netif)
{
    fnet_eth_if_t* ifptr;
    fnet_xemac_if_t* ethif;
    fnet_uint16 data;
    int result = 0;

    ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    ethif = ifptr ? ifptr->if_cpu_ptr : NULL;

    FNET_ASSERT(ethif != NULL);

    /* Some PHY (e.g.DP8340) returns "unconnected" and than "connected" state
     *  just to show that was transition event from one state to another.
     *  As we need only current state,  read 2 times and return
     *  the current/latest state.
     */
    fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_SR, &data);

    if (fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_SR, &data) == FNET_OK)
    {
        result = (data & FNET_ETH_MII_REG_SR_LINK_STATUS) ? 1 : 0;
    }

    return result;
}

/************************************************************************
* NAME: fnet_eth_mii_print_regs
*
* DESCRIPTION: Prints all MII registers.
* !!!! Used only for debug needs. !!!!!
*************************************************************************/
void fnet_eth_debug_mii_print_regs(fnet_shell_desc_t desc, fnet_netif_t *netif, fnet_uint32 flags)
{
    fnet_xemac_if_t *ethif;
    fnet_uint16 reg_value;
    fnet_uint32 reg32_value, addr, i;

    fnet_xemac_get_statistics(netif, NULL);

    if(netif->api->type == FNET_NETIF_TYPE_ETHERNET)
    {
        ethif = ((fnet_eth_if_t *)(netif->if_ptr))->if_cpu_ptr;
        FNET_ASSERT(ethif != NULL);

        if(flags & FNET_PHY_DBG_PRINT_REGS)
        {
        	fnet_shell_printf(desc, " === MII registers ===\n");

#define PRINT_MII_REG(reg) \
			fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_##reg, &reg_value); \
			fnet_shell_printf(desc, "\t[%2d] " #reg " = 0x%04X\n", FNET_ETH_MII_REG_##reg, reg_value );

#define PRINT_88E1510_REG(reg, pg) \
			fnet_xemac_mii_read(ethif, FNET_PHY88E1510_MII_REG_P##pg##_##reg, &reg_value); \
			fnet_shell_printf(desc, "\t[%2d_%d] " #reg " = 0x%04X\n", FNET_PHY88E1510_MII_REG_P##pg##_##reg, pg, reg_value );

			PRINT_MII_REG(CR);
			PRINT_MII_REG(SR);
			PRINT_MII_REG(IDR1);
			PRINT_MII_REG(IDR2);
			PRINT_MII_REG(AN_ADVERT);
			PRINT_MII_REG(AN_PABILS1);
			PRINT_MII_REG(AN_ER);
			PRINT_MII_REG(AN_NEXT_PGTX);
			PRINT_MII_REG(AN_PABILS2);
			PRINT_MII_REG(AN_ADVERTG);
			PRINT_MII_REG(AN_PABILS3);
			PRINT_MII_REG(EXTSR);
			PRINT_MII_REG(VENDOR_CR);
			PRINT_MII_REG(VENDOR_SR);
			PRINT_MII_REG(VENDOR_SR2);
			PRINT_MII_REG(RXERR_CTR);

		    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 1) == FNET_OK)
		    {

		    	PRINT_88E1510_REG(EX_SR, 1);
		    }

		    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 2) == FNET_OK)
		    {
		    	PRINT_88E1510_REG(MAC_CR, 2);
		    	PRINT_88E1510_REG(MAC_CR2, 2);
		    	PRINT_88E1510_REG(RGMII_CALIB_CTL, 2);
		    	PRINT_88E1510_REG(RGMII_CALIB_TGT, 2);
		    }

		    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 5) == FNET_OK)
		    {
		    	PRINT_88E1510_REG(PSKEW_POL, 5);
		    	PRINT_88E1510_REG(PSWAP_POL, 5);
		    }

		    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 6) == FNET_OK)
		    {
		    	PRINT_88E1510_REG(CRC_CTR, 6);
		    	PRINT_88E1510_REG(LCOL_CTR12, 6);
		    	PRINT_88E1510_REG(LCOL_CTR34, 6);
		    	PRINT_88E1510_REG(LCOL_WND, 6);
		    	PRINT_88E1510_REG(MISC_TEST, 6);
				PRINT_88E1510_REG(MISC_TEMP, 6);
		    }

		    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 7) == FNET_OK)
		    {
		    	PRINT_88E1510_REG(CDIAG_PAIR0, 7);
		    	PRINT_88E1510_REG(CDIAG_PAIR1, 7);
		    	PRINT_88E1510_REG(CDIAG_PAIR2, 7);
		    	PRINT_88E1510_REG(CDIAG_PAIR3, 7);
		    	PRINT_88E1510_REG(CDIAG_RSLT, 7);
		    	PRINT_88E1510_REG(CDIAG_CTL, 7);
		    }

		    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 18) == FNET_OK)
		    {
		    	PRINT_88E1510_REG(CRC_CTR, 18);
		    	PRINT_88E1510_REG(CRC_CTL, 18);
				PRINT_88E1510_REG(GCR1, 18);
		    	PRINT_88E1510_REG(LDISC_CTR, 18);
		    }

		    fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 0);
        }

        if(flags & FNET_PHY_DBG_PRINT_MAC)
        {
			fnet_shell_printf(desc, "\n=== GEM registers ===\n");

#define PRINT_GEM_REG(reg) \
			reg32_value = fnet_xemac_getreg(ethif, XEMACPS_##reg##_OFFSET); \
			fnet_shell_printf(desc, "\t[+0x%04x] " #reg " = 0x%08X\n", XEMACPS_##reg##_OFFSET, reg32_value )

			PRINT_GEM_REG(NWCTRL);
			PRINT_GEM_REG(NWCFG);
			PRINT_GEM_REG(NWSR);
			PRINT_GEM_REG(DMACR);
			PRINT_GEM_REG(TXSR);
			PRINT_GEM_REG(RXQBASE);
			PRINT_GEM_REG(TXQBASE);
			PRINT_GEM_REG(RXSR);
			PRINT_GEM_REG(ISR);
			PRINT_GEM_REG(IER);
			PRINT_GEM_REG(IDR);
			PRINT_GEM_REG(IMR);
			PRINT_GEM_REG(PHYMNTNC);
			PRINT_GEM_REG(RXPAUSE);
			PRINT_GEM_REG(TXPAUSE);
			PRINT_GEM_REG(HASHL);
			PRINT_GEM_REG(HASHH);
			PRINT_GEM_REG(LADDR1L);
			PRINT_GEM_REG(LADDR1H);
			PRINT_GEM_REG(LADDR2L);
			PRINT_GEM_REG(LADDR2H);
			PRINT_GEM_REG(LADDR3L);
			PRINT_GEM_REG(LADDR3H);
			PRINT_GEM_REG(LADDR4L);
			PRINT_GEM_REG(LADDR4H);
			PRINT_GEM_REG(MATCH1);
			PRINT_GEM_REG(MATCH2);
			PRINT_GEM_REG(MATCH3);
			PRINT_GEM_REG(MATCH4);
			PRINT_GEM_REG(STRETCH);
        }

        if(flags & FNET_PHY_DBG_PRINT_CPU)
        {
			fnet_shell_printf(desc, "\n=== SLCR registers ===\n");

			reg32_value = Xil_In32(addr = ethif->pparams->slcr_gem_clk_ctrl_addr);
			fnet_shell_printf(desc, "\t[%08x] SLCR_GEM_CLK_CTRL  = 0x%08X\n", addr, reg32_value );
			reg32_value = Xil_In32(addr = ethif->pparams->slcr_gem_clk_ctrl_addr-8);
			fnet_shell_printf(desc, "\t[%08x] SLCR_GEM_RCLK_CTRL = 0x%08X\n", addr, reg32_value );
			reg32_value = Xil_In32(addr = XPS_SYS_CTRL_BASEADDR + 0x214);
			fnet_shell_printf(desc, "\t[%08x] SLCR_GEM_RST_CTRL  = 0x%08X\n", addr, reg32_value );
			reg32_value = Xil_In32(addr = XPS_SYS_CTRL_BASEADDR + 0x12c);
			fnet_shell_printf(desc, "\t[%08x] SLCR_APER_CLK_CTRL = 0x%08X\n", addr, reg32_value );

			for(i=0; i<=53; i++)
			{
				reg32_value = Xil_In32(addr = XPS_SYS_CTRL_BASEADDR + 0x700 + i*4);
				fnet_shell_printf(desc, "\t[%08x] SLCR_MIO_PIN_%02d = 0x%08X\n", addr, i, reg32_value );
			}
        }

        if(flags & FNET_PHY_DBG_PRINT_STAT)
        {
#define PRINT_STAT(memb) \
			fnet_shell_printf(desc, "\t " #memb " \t = %u", ethif->mib_total.memb); \
			if(ethif->mib_delta.memb > 0) fnet_shell_printf(desc, " \tdelta=%u\n", ethif->mib_delta.memb); \
			else fnet_shell_printf(desc, "\n");

			fnet_shell_printf(desc, "\n === EMAC TX statistics ===\n");
			PRINT_STAT(tx_all);
			PRINT_STAT(tx_bcast);
			PRINT_STAT(tx_mcast);
			PRINT_STAT(tx_pause);
			PRINT_STAT(tx_64);
			PRINT_STAT(tx_64_127);
			PRINT_STAT(tx_128_255);
			PRINT_STAT(tx_256_511);
			PRINT_STAT(tx_512_1023);
			PRINT_STAT(tx_1024_1518);
			PRINT_STAT(tx_over_1519);
			PRINT_STAT(tx_err_underrun);
			PRINT_STAT(tx_err_single_col);
			PRINT_STAT(tx_err_multi_col);
			PRINT_STAT(tx_err_excess_col);
			PRINT_STAT(tx_err_late_col);
			PRINT_STAT(tx_err_deferred);
			PRINT_STAT(tx_err_cs);

			fnet_shell_printf(desc, "\n === EMAC RX statistics ===\n");
			PRINT_STAT(rx_all);
			PRINT_STAT(rx_bcast);
			PRINT_STAT(rx_mcast);
			PRINT_STAT(rx_pause);
			PRINT_STAT(rx_64);
			PRINT_STAT(rx_64_127);
			PRINT_STAT(rx_128_255);
			PRINT_STAT(rx_256_511);
			PRINT_STAT(rx_512_1023);
			PRINT_STAT(rx_1024_1518);
			PRINT_STAT(rx_over_1519);
			PRINT_STAT(rx_err_undersize);
			PRINT_STAT(rx_err_oversize);
			PRINT_STAT(rx_err_jabbers);
			PRINT_STAT(rx_err_fcs);
			PRINT_STAT(rx_err_len);
			PRINT_STAT(rx_err_symb);
			PRINT_STAT(rx_err_align);
			PRINT_STAT(rx_err_res);
			PRINT_STAT(rx_err_ovr);
			PRINT_STAT(rx_err_ipcs);
			PRINT_STAT(rx_err_tcpcs);
			PRINT_STAT(rx_err_udpcs);
        }
    }
}

#if FNET_CFG_MULTICAST
/************************************************************************
* NAME: fnet_xemac_multicast_join
*
* DESCRIPTION: Joins a multicast group on FEC interface.
*************************************************************************/
void fnet_xemac_multicast_join(fnet_netif_t *netif, fnet_mac_addr_t multicast_addr)
{
    fnet_eth_if_t* ifptr;
    fnet_xemac_if_t* ethif;
    int xresult, result;

    ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    ethif = ifptr ? ifptr->if_cpu_ptr : NULL;

    FNET_ASSERT(ethif != NULL);

    /* need to stop before changing HASH table address */
    if(ethif->xemacps_low.IsStarted == XIL_COMPONENT_IS_STARTED)
        XEmacPs_Stop(&ethif->xemacps_low);

    /* set the new MAC address */
    xres = XEmacPs_SetHash(&ethif->xemacps_low, multicast_addr);
    result = xres == XST_SUCCESS ? FNET_OK : FNET_ERR;

    /* if we should be running, re-start the MAC again. */
    if(result == FNET_OK && ethif->is_run_request)
        fnet_xemac_start_low(ethif);
}

/************************************************************************
* NAME: fnet_xemac_multicast_leave
*
* DESCRIPTION: Leavess a multicast group on FEC interface.
*************************************************************************/
void fnet_xemac_multicast_leave(fnet_netif_t *netif, fnet_mac_addr_t multicast_addr)
{
    fnet_eth_if_t* ifptr;
    fnet_xemac_if_t* ethif;
    int xresult, result;

    ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    ethif = ifptr ? ifptr->if_cpu_ptr : NULL;

    FNET_ASSERT(ethif != NULL);

    /* need to stop before changing HASH table address */
    if(ethif->xemacps_low.IsStarted == XIL_COMPONENT_IS_STARTED)
        XEmacPs_Stop(&ethif->xemacps_low);

    /* set the new MAC address */
    xres = XEmacPs_DeleteHash(&ethif->xemacps_low, multicast_addr);
    result = xres == XST_SUCCESS ? FNET_OK : FNET_ERR;

    /* if we should be running, re-start the MAC again. */
    if(result == FNET_OK && ethif->is_run_request)
        fnet_xemac_start_low(ethif);
}
#endif /* FNET_CFG_MULTICAST */


/* callable from link service */

int fnet_eth_check_media_status(fnet_netif_t *netif, fnet_uint32 *mac_changed)
{
    fnet_eth_if_t* ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    fnet_xemac_if_t* ethif = ifptr ? ifptr->if_cpu_ptr : NULL;

    if(!ethif)
        return FNET_ERR;

    fnet_eth_phy_update_mac_mode(ethif, mac_changed);
    return FNET_OK;
}

int fnet_eth_check_link_and_fix(fnet_netif_desc_t *netif_desc, int* ane_restarted, int reset_phy_on_linkdown)
{
    fnet_netif_t *netif = (fnet_netif_t*) netif_desc;
    fnet_eth_if_t* ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    fnet_xemac_if_t* ethif = ifptr ? ifptr->if_cpu_ptr : NULL;
    fnet_uint16 status = 0;
    fnet_uint16 status_2nd;
    int result = FNET_ERR;
    int restarted = FNET_ETH_LINK_OK_NOCHANGE;

    if(ane_restarted)
        *ane_restarted = FNET_ETH_LINK_OK_NOCHANGE;

    if(!ethif)
        return FNET_ERR;

    if (fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_SR, &status) == FNET_ERR)
        return FNET_ERR;

    if(!(status & FNET_ETH_MII_REG_SR_LINK_STATUS))
    {
        /* link is (or was) down, remember we should re-init as soon as possible */
        ethif->status_flags |= FNET_XEMACIFF_LINK_WAS_DOWN;

        /* read 2nd time to get fresh status */
        if(fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_SR, &status_2nd) == FNET_OK)
            status = status_2nd;
    }

    /* is link up ? */
    if(status & FNET_ETH_MII_REG_SR_LINK_STATUS)
    {
        /* yes, link is up, should we re-init? */
        if(ethif->status_flags & FNET_XEMACIFF_LINK_WAS_DOWN)
        {
            restarted = FNET_ETH_LINK_RESTORED;
            result = fnet_eth_check_media_status(netif, NULL);

            /* link just restored */
            ethif->status_flags &= ~FNET_XEMACIFF_LINK_WAS_DOWN;
        }
        else
        {
            /* link is still up, just verify the MAC reflects the PHY settings (only if autonegotiation is complete) */
            if(status & FNET_ETH_MII_REG_SR_AN_COMPLETE)
            {
                fnet_uint32 mac_changed = 0;
                result = fnet_eth_check_media_status(netif, &mac_changed);
                restarted = mac_changed ? FNET_ETH_LINK_OK_MACUPDATED : FNET_ETH_LINK_OK_NOCHANGE;
            }
            else
            {
                /* nothing to do */
                result = FNET_OK;
            }
        }
    }
    else
    {
        /* link is (or was) down, remember we should re-init as soon as possible */
        ethif->status_flags |= FNET_XEMACIFF_LINK_WAS_DOWN;

        if(reset_phy_on_linkdown)
        {
        	restarted = FNET_ETH_LINK_DOWN_PHYRESET;
        	result = fnet_eth_phy_init(ethif, 1);
        }
        else
        {
			restarted = FNET_ETH_LINK_DOWN_NOACTION;
			result = FNET_OK;
        }
    }

    if(ane_restarted)
        *ane_restarted = restarted;

    return result;
}

/************************************************************************
* NAME: fnet_xemac_isr_rx_handler_top
*
* DESCRIPTION: Top Ethernet receive frame interrupt handler.
*              Clear event flag
*************************************************************************/
static void fnet_xemac_isr_rx_handler_top (void *cookie)
{
    fnet_netif_t* netif = (fnet_netif_t*)cookie;
    fnet_eth_if_t* ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    fnet_xemac_if_t* ethif = ifptr ? ifptr->if_cpu_ptr : NULL;

    if(ethif)
    {
        /* Although ISR handling is quite simple, we use low level driver here
         * to make use of errata handling. Unfortunatelly, low-level handler
         * wants to call three callbacks - even if we do not need them really. */
        XEmacPs_IntrHandler(&ethif->xemacps_low);
    }
}

static void fnet_xemac_isr_callback_dmasend(void* param)
{
    /* no action needed */
}

static void fnet_xemac_isr_callback_dmarecv(void* param)
{
    /* no action needed */
}

static void fnet_xemac_isr_callback_error(void* param)
{
    /* no action needed */
}

/************************************************************************
* NAME: fnet_xemac_isr_rx_handler_bottom
*
* DESCRIPTION: This function implements the Ethernet receive
*              frame interrupt handler.
*************************************************************************/
static void fnet_xemac_isr_rx_handler_bottom (void *cookie)
{
    fnet_netif_t *netif = (fnet_netif_t *)cookie;

    if(netif)
    {
        fnet_isr_lock();
        fnet_xemac_input(netif);
        fnet_isr_unlock();
    }
}

/************************************************************************
* NAME: fnet_xemac_getreg
*
* DESCRIPTION: Read EMAC peripheral register.
*************************************************************************/
static fnet_uint32 fnet_xemac_getreg(fnet_xemac_if_t* ethif, fnet_uint32 byte_offset)
{
    return XEmacPs_ReadReg(ethif->xemacps_low.Config.BaseAddress, byte_offset);
}

/************************************************************************
* NAME: fnet_eth_phy_wait_ane_complete
*
* DESCRIPTION: Wait for auto-negotiation to complete
*************************************************************************/
static int fnet_eth_phy_wait_ane_complete(fnet_xemac_if_t *ethif, fnet_uint16* ret_status, fnet_uint16* ret_control)
{
    int result = FNET_ERR;
    fnet_uint16 status, control;
    unsigned long last_time = 0;

    if (fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_CR, &control) == FNET_ERR)
        control = 0;
    if (fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_SR, &status) == FNET_ERR)
        status = 0;

    /* Do we have auto-negotiate ability? */
    if ((status & FNET_ETH_MII_REG_SR_AN_ABILITY) && (control & FNET_ETH_MII_REG_CR_AN_ENABLE))
    {
        if(ethif->pparams->autoneg_timeout > 0)
        {
            /* wait for AN complete or for timeout */
            last_time = fnet_timer_ticks();
            do
            {
                /* Is auto-negotiation complete? */
                if (status & FNET_ETH_MII_REG_SR_AN_COMPLETE)
                {
                    result = FNET_OK;
                    break;
                }
            } while ( (fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_SR, &status) == FNET_OK)
                   && (fnet_timer_get_interval(last_time, fnet_timer_ticks()) <= ((ethif->pparams->autoneg_timeout)/FNET_TIMER_PERIOD_MS) ) );
        }
        else
        {
            /* no waiting, say it is OK if AN is enabled and complete*/
            if (status & FNET_ETH_MII_REG_SR_AN_COMPLETE)
                result = FNET_OK;
        }
    }

    if(ret_status)
        *ret_status = status;
    if(ret_control)
        *ret_control = control;

    return result;
}

/************************************************************************
* NAME: fnet_eth_phy_init
*
* DESCRIPTION: Ethernet Physical Transceiver initialization and/or reset.
*************************************************************************/
static int fnet_eth_phy_init(fnet_xemac_if_t *ethif, int force_reset)
{
    fnet_uint16 reg_value;
    int result = FNET_OK;

    /* extra software reset needed? */
    if((ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_INIT_RESET) || force_reset)
    {
        int i;

        /* set normal operation with auto-negotiation enabled */
        if(fnet_xemac_mii_write(ethif, FNET_ETH_MII_REG_CR, FNET_ETH_MII_REG_CR_RESET) != FNET_OK)
            result = FNET_ERR;

        /* wait for reset to complete, read ID register and expect sane value
         * Reset should finish in reasonable time (within 16 MII register reads)  */
        for(i=0; i<16; i++)
        {
            fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_IDR1, &reg_value);

            if (!(reg_value == 0 || reg_value == 0xffff || reg_value == 0x7fff))
            {
                /* seems like valid PHY indication */
                break;
            }
        }
    }


    /* set normal operation with auto-negotiation enabled */
    if(fnet_xemac_mii_write(ethif, FNET_ETH_MII_REG_CR, FNET_ETH_MII_REG_CR_AN_ENABLE) != FNET_OK)
        result = FNET_ERR;

    /* advertise our 10 and 100 capabilities */
    if(fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_AN_ADVERT, &reg_value) == FNET_OK)
    {
        /* set flags according to user configuration */
        reg_value &= ~(FNET_ETH_MII_REG_ANADV_ASYMMETRIC_PAUSE |
                       FNET_ETH_MII_REG_ANADV_PAUSE |
                       FNET_ETH_MII_REG_ANADV_10 |
                       FNET_ETH_MII_REG_ANADV_100);

        if(ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_PAUSE)
                reg_value |= FNET_ETH_MII_REG_ANADV_PAUSE;
        if(ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_ASYM_PAUSE)
            reg_value |= FNET_ETH_MII_REG_ANADV_ASYMMETRIC_PAUSE;
        if(ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_10)
            reg_value |= FNET_ETH_MII_REG_ANADV_10;
        if(ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_100)
            reg_value |= FNET_ETH_MII_REG_ANADV_100;

        if(fnet_xemac_mii_write(ethif, FNET_ETH_MII_REG_AN_ADVERT, reg_value) != FNET_OK)
            result = FNET_ERR;
    }
    else
    {
        result = FNET_ERR;
    }

    /* advertise our 1000 capabilities */
    if(fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_AN_ADVERTG, &reg_value) == FNET_OK)
    {
      /* set flags according to user configuration */
        reg_value &= ~(FNET_ETH_MII_REG_ANADVG_1000HALF | FNET_ETH_MII_REG_ANADVG_1000FULL);
      
        if(ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_1000HALF)
            reg_value |= FNET_ETH_MII_REG_ANADVG_1000HALF;
        if(ethif->pparams->autoneg_advertise & FNET_ETH_PHY_MEDIA_1000FULL)
            reg_value |= FNET_ETH_MII_REG_ANADVG_1000FULL;

        if(fnet_xemac_mii_write(ethif, FNET_ETH_MII_REG_AN_ADVERTG, reg_value) != FNET_OK)
            result = FNET_ERR;
    }
    else
    {
        result = FNET_ERR;
    }

    /* start auto-negotiation */
    if(fnet_xemac_mii_write(ethif, FNET_ETH_MII_REG_CR, FNET_ETH_MII_REG_CR_AN_ENABLE | FNET_ETH_MII_REG_CR_AN_RESTART) != FNET_OK)
        result = FNET_ERR;

    /* wait for auto-negotiation and setup MAC */
    if(fnet_eth_phy_update_mac_mode(ethif, NULL) != FNET_OK)
        result = FNET_ERR;

    return result;
}

/* Update MAC according to the current PHY media status (clock and duplex) */

static int fnet_eth_phy_update_mac_mode(fnet_xemac_if_t* ethif, fnet_uint32* mac_changed)
{
    fnet_uint32 media_status = 0;
    fnet_uint32 changed = 0;
    int result;

    /* get negotiated speed */
    if(fnet_eth_phy_get_media_status(ethif, &media_status) == FNET_OK)
    {
        /* reconfigure MAC clock */
        fnet_xemac_set_clock_and_duplex(ethif, media_status, &changed);
        result = FNET_OK;
    }
    else
    {
        result = FNET_ERR;
    }

    if(mac_changed)
        *mac_changed = changed;

    return result;
}

/* Get auto-negotiated speed mode. This function also returns fixed speed if auto-negotiation fails */

static int fnet_eth_phy_get_media_status(fnet_xemac_if_t* ethif, fnet_uint32* media_status)
{
    fnet_uint16 status, control;
    fnet_uint16 partner_abils;
    fnet_uint16 my_abils;

    *media_status = 0;

    /* auto-negotiation results valid? */
    if(fnet_eth_phy_wait_ane_complete(ethif, &status, &control) == FNET_OK)
    {
        /* extended status available? */
        if (status & FNET_ETH_MII_REG_SR_EXTSTAT)
        {
            /* determine common features we have with our link parent */
            if(fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_AN_PABILS3, &partner_abils) == FNET_OK &&
               fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_AN_ADVERTG, &my_abils) == FNET_OK)
            {
                if((partner_abils & FNET_ETH_MII_REG_PABILS3_1000FULL) && (my_abils & FNET_ETH_MII_REG_ANADVG_1000FULL))
                    *media_status |= FNET_ETH_PHY_MEDIA_1000FULL;
                if((partner_abils & FNET_ETH_MII_REG_PABILS3_1000HALF) && (my_abils & FNET_ETH_MII_REG_ANADVG_1000HALF))
                    *media_status |= FNET_ETH_PHY_MEDIA_1000HALF;
            }
        }

        /* determine common features we have with our link parent */
        if(fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_AN_PABILS1, &partner_abils) == FNET_OK &&
           fnet_xemac_mii_read(ethif, FNET_ETH_MII_REG_AN_ADVERT, &my_abils) == FNET_OK)
        {
            if ((partner_abils & FNET_ETH_MII_REG_PABILS1_100FULL) && (my_abils & FNET_ETH_MII_REG_ANADV_100FULL))
                *media_status |= FNET_ETH_PHY_MEDIA_100FULL;
            if ((partner_abils & FNET_ETH_MII_REG_PABILS1_100HALF) && (my_abils & FNET_ETH_MII_REG_ANADV_100HALF))
                *media_status |= FNET_ETH_PHY_MEDIA_100HALF;
            if ((partner_abils & FNET_ETH_MII_REG_PABILS1_10FULL) && (my_abils & FNET_ETH_MII_REG_ANADV_10FULL))
                *media_status |= FNET_ETH_PHY_MEDIA_10FULL;
            if ((partner_abils & FNET_ETH_MII_REG_PABILS1_10HALF) && (my_abils & FNET_ETH_MII_REG_ANADV_10HALF))
                *media_status |= FNET_ETH_PHY_MEDIA_10HALF;
        }
    }
    /* auto-negotiation not usable, get preset configuration */
    else
    {
        if(status & FNET_ETH_MII_REG_SR_EXTSTAT)
        {
            /* Get commanded link speed */
            switch(control & FNET_ETH_MII_REG_CR_SPEED_MASK)
            {
            case FNET_ETH_MII_REG_CR_SPEED_1000M:
                *media_status |= FNET_ETH_PHY_MEDIA_1000;
                break;
            case FNET_ETH_MII_REG_CR_SPEED_100M:
                *media_status |= FNET_ETH_PHY_MEDIA_100;
                break;
            case FNET_ETH_MII_REG_CR_SPEED_10M:
                *media_status |= FNET_ETH_PHY_MEDIA_10;
                break;
            default:
                break;
            }
        }
    }

    return *media_status ? FNET_OK : FNET_ERR;
}


static void fnet_xemac_set_clock_and_duplex(fnet_xemac_if_t* ethif, fnet_uint32 media_status, fnet_uint32* mac_changed)
{
    fnet_uint32 new_gem;
    fnet_uint32 old_gem;
    fnet_uint32 full_duplex;
    fnet_uint32 changed = 0;
    int speed;

    FNET_ASSERT(ethif->pparams->slcr_gem_clk_ctrl_addr != 0);
    old_gem = Xil_In32(ethif->pparams->slcr_gem_clk_ctrl_addr);

    if(media_status & FNET_ETH_PHY_MEDIA_1000)
    {
        new_gem = ethif->pparams->slcr_reg_1000;
        full_duplex = media_status & FNET_ETH_PHY_MEDIA_1000FULL;
        speed = 1000;
    }
    else if(media_status & FNET_ETH_PHY_MEDIA_100)
    {
        new_gem = ethif->pparams->slcr_reg_100;
        full_duplex = media_status & FNET_ETH_PHY_MEDIA_100FULL;
        speed = 100;
    }
    else
    {
        new_gem = ethif->pparams->slcr_reg_10;
        full_duplex = media_status & FNET_ETH_PHY_MEDIA_10FULL;
        speed = 10;
    }

    /* need to change gem? */
    if(new_gem != old_gem)
    {
        /* SLCR unlock */
        Xil_Out32(SLCR_UNLOCK_ADDR, SLCR_UNLOCK_KEY_VALUE);
        /* register change */
        Xil_Out32(ethif->pparams->slcr_gem_clk_ctrl_addr, new_gem);
        /* SLCR lock */
        Xil_Out32(SLCR_LOCK_ADDR, SLCR_LOCK_KEY_VALUE);
        /* MAC changed */
        changed = 1;
    }

    /* need to set operating speed? */
    if(XEmacPs_GetOperatingSpeed(&ethif->xemacps_low) != speed)
    {
        XEmacPs_SetOperatingSpeed(&ethif->xemacps_low, speed);

        /* MAC changed */
        changed = 2;
    }

    if(mac_changed)
        *mac_changed = changed;

    /* TODO: duplex */
}

int fnet_phy88e1510_set_impedance_calibration(fnet_netif_desc_t *netif_desc, fnet_phy88e1510_calibration_t* calib)
{
    fnet_netif_t *netif = (fnet_netif_t*) netif_desc;
    fnet_eth_if_t* ifptr = netif ? (fnet_eth_if_t *)(netif->if_ptr) : NULL;
    fnet_xemac_if_t* ethif = ifptr ? ifptr->if_cpu_ptr : NULL;
    fnet_uint16 status = 0;
    fnet_int32 retry;

    int result = FNET_ERR;
    calib->result = 0;
    calib->previous = 0;

    if(!ethif)
        return FNET_ERR;

    if(!calib)
        return FNET_ERR;

    if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 2) == FNET_ERR)
        return FNET_ERR;

    /* get actual status value */
    if (fnet_xemac_mii_read(ethif, FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_CTL, &status) == FNET_OK)
        calib->previous = status;

    /* automatic calibration ?*/
    if(calib->automatic_target & FNET_PHY_88E1510_CALIB_AUTO_FLAG)
    {
        /* prepare calibration target value  */
        if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_TGT, calib->automatic_target & 0x7) == FNET_ERR)
            goto reset_page;

        /* start calibration */
        if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_CTL, 0x8000) == FNET_ERR)
            goto reset_page;

        /* wait for completion */
        for(retry=0; retry < 100; retry++)
        {
            if (fnet_xemac_mii_read(ethif, FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_CTL, &status) == FNET_ERR)
                continue;

            calib->result = status;

            /* finished? */
            if(status & 0x4000)
            {
                result = FNET_OK;
                break;
            }
        }
    }
    else
    {
        fnet_uint16 control = 0x40; /* manual calibration flag */
        control |= (calib->pmos << 8) & 0x0f00;
        control |= (calib->nmos << 0) & 0x000f;

        /* manual calibration */
        if (fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_CTL, control) == FNET_ERR)
            goto reset_page;

        if (fnet_xemac_mii_read(ethif, FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_CTL, &status) == FNET_ERR)
            goto reset_page;

        calib->result = status;
        result = FNET_OK;
    }

reset_page:
    /* restore page register */
    fnet_xemac_mii_write(ethif, FNET_PHY88E1510_MII_REG_PAGE, 0);
    return result;
}

#endif /* FNET_XC7Z && FNET_CFG_ETH */



