/**************************************************************************
* 
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_mk_timer.c
*
* @author Andrey Butok
*
* @date Aug-2-2012
*
* @version 0.1.7.0
*
* @brief Kinetis specific SW timers implementation.
*
***************************************************************************/

#include "fnet_config.h"

#if FNET_XC7Z

#include "fnet.h"
#include "fnet_timer_prv.h"
#include "fnet_isr.h"

XTtcPs*  fnet_xc7z_ttc = NULL;

/************************************************************************
* NAME: fnet_timer_handler_top
*
* DESCRIPTION: Top interrupt handler. Increment fnet_current_time 
*              and interrupt flag. 
*************************************************************************/
static void fnet_cpu_timer_handler_top( void *cookie )
{
    FNET_ASSERT(cookie == fnet_xc7z_ttc);

    /* acknowledge TTC interrupt */
    if(fnet_xc7z_ttc)
    {
        u32 istat = XTtcPs_GetInterruptStatus(fnet_xc7z_ttc);
        XTtcPs_ClearInterruptStatus(fnet_xc7z_ttc, istat);
    }

    /* Update RTC counter. */
    fnet_timer_ticks_inc(); 
}

/************************************************************************
* NAME: fnet_cpu_timer_init
*
* DESCRIPTION: Starts TCP/IP hardware timer. period of timer (ms)

*************************************************************************/
int fnet_cpu_timer_init( unsigned int period_ms )
{
    static XTtcPs fnet_xc7z_ttc_instance;

    int result = FNET_OK;
    fnet_uint32 timeout;
    XTtcPs_Config* pcfg;

    /* user might have set another TTC instance before starting FNET */
    if(!fnet_xc7z_ttc)
    {
        fnet_xc7z_ttc = &fnet_xc7z_ttc_instance;
        pcfg = XTtcPs_LookupConfig(FNET_CFG_CPU_TIMER_NUMBER);
        if(XTtcPs_CfgInitialize(fnet_xc7z_ttc, pcfg, pcfg->BaseAddress) != XST_SUCCESS)
        {
            /* workaround to make initialization to complete */
            XTtcPs_Stop(fnet_xc7z_ttc);
            if(XTtcPs_CfgInitialize(fnet_xc7z_ttc, pcfg, pcfg->BaseAddress) == XST_SUCCESS)
                result = FNET_OK;
            else
                result = FNET_ERR;
        }
    }

    if(result == FNET_OK)
    {
        if(fnet_xc7z_ttc != NULL)
        {
            /* Install and enable interrupt  */
            result = fnet_isr_vector_init(FNET_CFG_CPU_TIMER_VECTOR_NUMBER, fnet_cpu_timer_handler_top,
                                              fnet_timer_handler_bottom, FNET_CFG_CPU_TIMER_VECTOR_PRIORITY,
                                              fnet_xc7z_ttc);
        }
        else
        {
            result = FNET_ERR;
        }
    }

    if(result == FNET_OK)
    {
        u16 interval;
        u8 prescaler;

        XTtcPs_SetOptions(fnet_xc7z_ttc, XTTCPS_OPTION_INTERVAL_MODE | XTTCPS_OPTION_WAVE_DISABLE);
        XTtcPs_CalcIntervalFromFreq(fnet_xc7z_ttc, period_ms ? ((1000 + period_ms/2) / period_ms) : 1000,
                &interval, &prescaler);

        if(interval != 0xffff && prescaler != 0xff)
        {
            XTtcPs_SetPrescaler(fnet_xc7z_ttc, prescaler);
            XTtcPs_SetInterval(fnet_xc7z_ttc, interval);

            XTtcPs_ResetCounterValue(fnet_xc7z_ttc);
            XTtcPs_ClearInterruptStatus(fnet_xc7z_ttc, XTTCPS_IXR_ALL_MASK);
            XTtcPs_EnableInterrupts(fnet_xc7z_ttc, XTTCPS_IXR_INTERVAL_MASK);

            XTtcPs_Start(fnet_xc7z_ttc);
        }
        else
        {
            result = FNET_ERR;
        }
    }
    
    return result;
}

/************************************************************************
* NAME: fnet_cpu_timer_release
*
* DESCRIPTION: Relaeses TCP/IP hardware timer.
*              
*************************************************************************/
void fnet_cpu_timer_release( void )
{
    if(fnet_xc7z_ttc != NULL)
    {
        XTtcPs_Stop(fnet_xc7z_ttc);
        XTtcPs_ClearInterruptStatus(fnet_xc7z_ttc, XTTCPS_IXR_ALL_MASK);
        XTtcPs_DisableInterrupts(fnet_xc7z_ttc, XTTCPS_IXR_ALL_MASK);
    }

    /* Uninstall interrupt handler.
     */
    fnet_isr_vector_release(FNET_CFG_CPU_TIMER_VECTOR_NUMBER);
}



#endif /*FNET_XC7Z*/
