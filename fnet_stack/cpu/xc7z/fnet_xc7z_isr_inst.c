/**************************************************************************
* 
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_mk_isr_inst.c
*
* @author Andrey Butok
*
* @date Aug-2-2012
*
* @version 0.1.8.0
*
* @brief Interrupt service dispatcher implementation.
*
***************************************************************************/

#include "fnet_config.h" 
#if FNET_XC7Z

#if !FNET_OS

#include "fnet.h"
#include "fnet_isr.h"
#include "fnet_timer.h"
#include "fnet_netbuf.h"

/************************************************************************
* NAME: fnet_cpu_isr_install
*
* DESCRIPTION: 
*************************************************************************/

void FNET_ISR_HANDLER(void*);

int fnet_cpu_isr_install(unsigned int vector_number, unsigned int priority)
{
    int result;
    u8 prio, trig;

    FNET_ASSERT(fnet_xc7z_gic != NULL);

    if(fnet_xc7z_gic)
    {
        XScuGic_Connect(fnet_xc7z_gic, vector_number, (Xil_ExceptionHandler) FNET_ISR_HANDLER, (void*)vector_number);
        XScuGic_GetPriorityTriggerType(fnet_xc7z_gic, vector_number, &prio, &trig);
        prio = priority << 5; /* FNET uses 3-bit priority range, ARM needs 8-bit */
        XScuGic_SetPriorityTriggerType(fnet_xc7z_gic, vector_number, prio, trig);
        XScuGic_Enable(fnet_xc7z_gic, vector_number);
        result = FNET_OK;
    }
    else
    {
        result = FNET_ERR;
    }
   
   return result;            
}

#endif /* !FNET_OS */
#endif /* FNET_XC7Z */
