/**************************************************************************
* 
* Copyright 2013
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_xc7z_eth.h
*
* @author Michal Hanak
*
* @date Aug-2-2012
*
* @version 0.1.11.0
*
* @brief XC7Z Ethernet Interface
*
***************************************************************************/

#ifndef _FNET_XC7Z_ETH_H_
#define _FNET_XC7Z_ETH_H_

#include "fnet_config.h"

#if FNET_XC7Z

#include "fnet.h"
#include "fnet_eth_prv.h"

/* required buffer alignment */
#define FNET_XEMAC_BDRING_ALIGNMENT 16  // must be higher than BD_ALIGNMENT
#define FNET_XEMAC_BD_ALIGNMENT     XEMACPS_BD_ALIGNMENT  // Do not change!
#define FNET_XEMAC_TXBUF_ALIGNMENT  16  // XEMACPS_TX_BUF_ALIGNMENT=1
#define FNET_XEMAC_RXBUF_ALIGNMENT  16  // XEMACPS_RX_BUF_ALIGNMENT=4

/* alignment macros  */
#define FNET_XEMAC_ALIGN_UP(align, ptr)   (((fnet_uint32)(ptr) + ((fnet_uint32)(align)-1)) & (~((fnet_uint32)(align)-1)))
#define FNET_XEMAC_ALIGN_DOWN(align, ptr) (((fnet_uint32)(ptr)) & (~((fnet_uint32)(align)-1)))

/* Frame buffer sizes in bytes.*/
#define FNET_XEMAC_BUF_SIZE        (((FNET_CFG_CPU_ETH0_MTU>FNET_CFG_CPU_ETH1_MTU)?FNET_CFG_CPU_ETH0_MTU:FNET_CFG_CPU_ETH1_MTU)+FNET_ETH_HDR_SIZE+FNET_ETH_CRC_SIZE+16)

/* Number of buffers from user config */
#define FNET_XEMAC_TX_BUF_NUM      (FNET_CFG_CPU_ETH_TX_BUFS_MAX)
#define FNET_XEMAC_RX_BUF_NUM      (FNET_CFG_CPU_ETH_RX_BUFS_MAX)

/* XEMAC Buffer Descriptor Rings are handled the FNET-way similar to Freescale FEC
 * The original Xilinx BDRING driver is unusable for our needs */
typedef struct
{
    fnet_vuint32 addr;  // word0 is address (combined with some ctrl bits in case of RX BD)
    fnet_vuint32 stat;  // word1 is control and status word
} fnet_xemac_bd_t;

/* Initialization parameters for XEMAC Module Control data structure */
typedef struct
{
    fnet_uint32        clk_mdio;          /* CLK_1x clock on MDIO input */
    fnet_uint32        phy_addr_init;     /* initial PHY address */
    fnet_uint32        phy_auto_discover; /* true if PHY should be auto-discovered */
    fnet_uint32        phy_clock_khz;     /* PHY MDIO communication speed */
    fnet_uint32        autoneg_advertise; /* combination of NET_ETH_PHY_MEDIA_xxx flags to advertise */
    fnet_uint32        autoneg_timeout;   /* autonegotioation timeout */
    fnet_uint32        vector_number;     /* Vector number of the Ethernet Receive Frame interrupt.*/
    fnet_uint32        slcr_gem_clk_ctrl_addr; /* address of SLCR register for this module */
    fnet_uint32        slcr_reg_1000;     /* SLCR_GEMx clock register for 1000M network */
    fnet_uint32        slcr_reg_100;      /* SLCR_GEMx clock register for 100M network */
    fnet_uint32        slcr_reg_10;       /* SLCR_GEMx clock register for 10M network */
} fnet_xemac_params_t;

/* Detailed MIB structure of the XEMAC maintained internally and translated to
 * fnet_netif_statistics when needed. */
typedef struct
{
    /* TX */
    fnet_uint32        tx_all;
    fnet_uint32        tx_bcast;
    fnet_uint32        tx_mcast;
    fnet_uint32        tx_pause;
    fnet_uint32        tx_64;
    fnet_uint32        tx_64_127;
    fnet_uint32        tx_128_255;
    fnet_uint32        tx_256_511;
    fnet_uint32        tx_512_1023;
    fnet_uint32        tx_1024_1518;
    fnet_uint32        tx_over_1519;
    fnet_uint32        tx_err_underrun;
    fnet_uint32        tx_err_single_col;
    fnet_uint32        tx_err_multi_col;
    fnet_uint32        tx_err_excess_col;
    fnet_uint32        tx_err_late_col;
    fnet_uint32        tx_err_deferred;
    fnet_uint32        tx_err_cs;

    /* RX */
    fnet_uint32        rx_all;
    fnet_uint32        rx_bcast;
    fnet_uint32        rx_mcast;
    fnet_uint32        rx_pause;
    fnet_uint32        rx_64;
    fnet_uint32        rx_64_127;
    fnet_uint32        rx_128_255;
    fnet_uint32        rx_256_511;
    fnet_uint32        rx_512_1023;
    fnet_uint32        rx_1024_1518;
    fnet_uint32        rx_over_1519;
    fnet_uint32        rx_err_undersize;
    fnet_uint32        rx_err_oversize;
    fnet_uint32        rx_err_jabbers;
    fnet_uint32        rx_err_fcs;
    fnet_uint32        rx_err_len;
    fnet_uint32        rx_err_symb;
    fnet_uint32        rx_err_align;
    fnet_uint32        rx_err_res;
    fnet_uint32        rx_err_ovr;
    fnet_uint32        rx_err_ipcs;
    fnet_uint32        rx_err_tcpcs;
    fnet_uint32        rx_err_udpcs;
} fnet_xemac_statistics_t;

/* Memory buffers for XEMAC Module - MUST RESIDE IN NON-CACHED MEMORY */
typedef struct
{
    /* static memory allocation */
    fnet_uint8 tx_bdring_mem[XEmacPs_BdRingMemCalc(FNET_XEMAC_BD_ALIGNMENT, FNET_XEMAC_TX_BUF_NUM) + FNET_XEMAC_BDRING_ALIGNMENT];
    fnet_uint8 rx_bdring_mem[XEmacPs_BdRingMemCalc(FNET_XEMAC_BD_ALIGNMENT, FNET_XEMAC_RX_BUF_NUM) + FNET_XEMAC_BDRING_ALIGNMENT];
    fnet_uint8 tx_bufs_mem[FNET_XEMAC_TX_BUF_NUM][FNET_XEMAC_BUF_SIZE + (FNET_XEMAC_TXBUF_ALIGNMENT-1)];
    fnet_uint8 rx_bufs_mem[FNET_XEMAC_RX_BUF_NUM][FNET_XEMAC_BUF_SIZE + (FNET_XEMAC_RXBUF_ALIGNMENT-1)];
} fnet_xemac_if_bufs_t;

#define FNET_XEMACIFF_IS_CONFIGURED  0x0001  /* initialized */
#define FNET_XEMACIFF_IS_RUN_REQUEST 0x0002  /* run pending */
#define FNET_XEMACIFF_LINK_WAS_DOWN  0x0004  /* link was recently down */

/* XEMAC Module Control data structure */
typedef struct
{
    /* initialization parameters  */
    const fnet_xemac_params_t* pparams;

    /* low level driver */
    XEmacPs            xemacps_low;       /* Xilinx low driver instance */
    XEmacPs          * xemacps_low_mii;   /* Xilinx low driver instance for MDIO access (ETH1 may use ETH0's MDIO) */
    fnet_xemac_statistics_t mib_delta;    /* Frame counters, the last delta added when fnet_xemac_get_statistics called */
    fnet_xemac_statistics_t mib_total;    /* Frame counters, totals, updated whenever fnet_xemac_get_statistics is called */

    /* configuration */
    fnet_uint32        tx_buf_desc_num;   /* Number of allocated Tx Buffer Descriptors.*/
    fnet_uint32        rx_buf_desc_num;   /* Number of allocated Tx Buffer Descriptors.*/
    fnet_uint32        mdio_clk_khz;      /* Real achieved PHY clock.*/
    fnet_mac_addr_t    local_mac_addr;    /* Assigned MAC address */
    fnet_uint32        phy_addr;          /* PHY addr - autodetected or assigned */

    /* interface status */
    fnet_uint32        status_flags;      /* various status flags, FNET_XEMACIFF_xxx */

    /* Ethernet buffers and buffer descriptor rings */
    fnet_xemac_bd_t *  tx_buf_desc;       /* Tx Buffer Descriptors.*/
    fnet_xemac_bd_t *  tx_buf_desc_cur;   /* Points to the descriptor of the current outcoming buffer.*/
    fnet_xemac_bd_t *  rx_buf_desc;       /* Rx Buffer Descriptors.*/
    fnet_xemac_bd_t *  rx_buf_desc_cur;   /* Points to the descriptor of the current incoming buffer.*/
    fnet_xemac_if_bufs_t * bufs;          /* Our data in un-cached memory */
} fnet_xemac_if_t;

/* XEMAC driver API */
extern const fnet_netif_api_t fnet_xemac_api;

/* Ethernet specific control data structure.*/
#if FNET_CFG_CPU_ETH0
    extern fnet_xemac_if_t fnet_xemac0_if;
#endif
#if FNET_CFG_CPU_ETH1
    extern fnet_xemac_if_t fnet_xemac1_if;
#endif

/************************************************************************
*     Function Prototypes
*************************************************************************/
int fnet_xemac_init(fnet_netif_t *netif);
void fnet_xemac_release(fnet_netif_t *netif);
void fnet_xemac_input(fnet_netif_t *netif);
int fnet_xemac_get_hw_addr(fnet_netif_t *netif, unsigned char * hw_addr);
int fnet_xemac_set_hw_addr(fnet_netif_t *netif, unsigned char * hw_addr);
int fnet_xemac_is_connected(fnet_netif_t *netif);
int fnet_xemac_get_statistics(struct fnet_netif *netif, struct fnet_netif_statistics * statistics);
void fnet_xemac_output(fnet_netif_t *netif, unsigned short type, const fnet_mac_addr_t dest_addr, fnet_netbuf_t* nb);

#if FNET_CFG_MULTICAST
void fnet_xemac_multicast_join(fnet_netif_t *netif, fnet_mac_addr_t multicast_addr);
void fnet_xemac_multicast_leave(fnet_netif_t *netif, fnet_mac_addr_t multicast_addr);
#endif /* FNET_CFG_MULTICAST */

/* For debug needs.*/
#define FNET_PHY_DBG_PRINT_REGS 0x0001
#define FNET_PHY_DBG_PRINT_MAC  0x0002
#define FNET_PHY_DBG_PRINT_CPU  0x0004
#define FNET_PHY_DBG_PRINT_STAT 0x0008
void fnet_eth_debug_mii_print_regs(fnet_shell_desc_t desc, fnet_netif_t *netif, fnet_uint32 flags);

void fnet_xemac_stop(fnet_netif_t *netif);
void fnet_xemac_resume(fnet_netif_t *netif);

/* Ethernet IO initialization.*/
void fnet_eth_io_init(void) ;

/********************************************************************************************
 * definitions specific to Marvell 88E1510 PHY
 */

#define FNET_PHY88E1510_MII_REG_PAGE             22

/* page 1 registers */
#define FNET_PHY88E1510_MII_REG_P1_EX_SR       15

/* page 2 registers */
#define FNET_PHY88E1510_MII_REG_P2_MAC_CR           16
#define FNET_PHY88E1510_MII_REG_P2_MAC_CR2          21
#define FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_CTL  24
#define FNET_PHY88E1510_MII_REG_P2_RGMII_CALIB_TGT  25

/* page 5 registers */
#define FNET_PHY88E1510_MII_REG_P5_PSKEW_POL   20
#define FNET_PHY88E1510_MII_REG_P5_PSWAP_POL   21

/* page 6 registers */
#define FNET_PHY88E1510_MII_REG_P6_CRC_CTR     17
#define FNET_PHY88E1510_MII_REG_P6_LCOL_CTR12  23
#define FNET_PHY88E1510_MII_REG_P6_LCOL_CTR34  24
#define FNET_PHY88E1510_MII_REG_P6_LCOL_WND    25
#define FNET_PHY88E1510_MII_REG_P6_MISC_TEST   26
#define FNET_PHY88E1510_MII_REG_P6_MISC_TEMP   27

/* page 7 registers */
#define FNET_PHY88E1510_MII_REG_P7_CDIAG_PAIR0 16
#define FNET_PHY88E1510_MII_REG_P7_CDIAG_PAIR1 17
#define FNET_PHY88E1510_MII_REG_P7_CDIAG_PAIR2 18
#define FNET_PHY88E1510_MII_REG_P7_CDIAG_PAIR3 19
#define FNET_PHY88E1510_MII_REG_P7_CDIAG_RSLT  20
#define FNET_PHY88E1510_MII_REG_P7_CDIAG_CTL   21

/* page 18 registers */
#define FNET_PHY88E1510_MII_REG_P18_CRC_CTR    17
#define FNET_PHY88E1510_MII_REG_P18_CRC_CTL    18
#define FNET_PHY88E1510_MII_REG_P18_GCR1       20
#define FNET_PHY88E1510_MII_REG_P18_LDISC_CTR  25


#define FNET_PHY_88E1510_CALIB_AUTO_FLAG 0x80000000
#define FNET_PHY_88E1510_CALIB_AUTO_78Z8 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x00)
#define FNET_PHY_88E1510_CALIB_AUTO_64Z5 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x01)
#define FNET_PHY_88E1510_CALIB_AUTO_54Z6 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x02)
#define FNET_PHY_88E1510_CALIB_AUTO_47Z3 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x03)
#define FNET_PHY_88E1510_CALIB_AUTO_41Z7 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x04)
#define FNET_PHY_88E1510_CALIB_AUTO_37Z3 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x05)
#define FNET_PHY_88E1510_CALIB_AUTO_33Z8 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x06)
#define FNET_PHY_88E1510_CALIB_AUTO_30Z9 (FNET_PHY_88E1510_CALIB_AUTO_FLAG | 0x07)

typedef struct
{
    fnet_uint32 automatic_target;  /* one of PHY_88E1510_CALIB_AUTO_xxx */

    /* manual settings (used when automatic_target is clear) */
    fnet_uint16 pmos;
    fnet_uint16 nmos;

    /* members filled upon return */
    fnet_uint16 previous;  /* previous value of CALIB_CTL */
    fnet_uint16 result;    /* final result (the CALIB_CTL register) */

} fnet_phy88e1510_calibration_t;

int fnet_phy88e1510_set_impedance_calibration(fnet_netif_desc_t *netif_desc, fnet_phy88e1510_calibration_t* calib);

#endif /* FNET_XC7Z */

#endif /*_FNET_XC7Z_ETH_H_*/
