/**************************************************************************
* 
* Copyright 2012 by Andrey Butok.
* Copyright 2005-2011 by Andrey Butok. Freescale Semiconductor, Inc.
*
***************************************************************************
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License Version 3 
* or later (the "LGPL").
*
* As a special exception, the copyright holders of the FNET project give you
* permission to link the FNET sources with independent modules to produce an
* executable, regardless of the license terms of these independent modules,
* and to copy and distribute the resulting executable under terms of your 
* choice, provided that you also meet, for each linked independent module,
* the terms and conditions of the license of that module.
* An independent module is a module which is not derived from or based 
* on this library. 
* If you modify the FNET sources, you may extend this exception 
* to your version of the FNET sources, but you are not obligated 
* to do so. If you do not wish to do so, delete this
* exception statement from your version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License
* and the GNU Lesser General Public License along with this program.
* If not, see <http://www.gnu.org/licenses/>.
*
**********************************************************************/ /*!
*
* @file fnet_XC7Z_config.h
*
* @author Andrey Butok
*
* @date Dec-17-2012
*
* @version 0.1.8.0
*
* @brief Kinetis specific default configuration.
*
***************************************************************************/

/************************************************************************
 * !!!DO NOT MODIFY THIS FILE!!!
 ************************************************************************/

#ifndef _FNET_XC7Z_CONFIG_H_
#define _FNET_XC7Z_CONFIG_H_

#include "fnet_user_config.h"

#ifndef FNET_XC7Z
#define FNET_XC7Z   (0)
#endif

#if FNET_XC7Z

/* this driver reuses Xilinx XPS-generated BSP */
#include "xparameters.h"
#include "xparameters_ps.h"

/* System frequency in Hz. */
#ifndef FNET_CFG_CPU_CLOCK_HZ
    #define FNET_CFG_CPU_CLOCK_HZ           (200000000UL)
#endif

/**************************************************************************
 *  Reduced Media Independent Interface (RMII) support.
 ******************************************************************************/
#ifndef FNET_CFG_CPU_ETH_RMII
    #define FNET_CFG_CPU_ETH_RMII        			(1)
#endif 

/**************************************************************************
 *  Default serial port number.
 ******************************************************************************/
#ifndef FNET_CFG_CPU_SERIAL_PORT_DEFAULT
    #define FNET_CFG_CPU_SERIAL_PORT_DEFAULT        (0)
#endif

/**************************************************************************
 *  Maximum Timer number that is available on the used platform.
 ******************************************************************************/
#define  FNET_CFG_CPU_TIMER_NUMBER_MAX              (2)

/******************************************************************************
 *  Timer used
 ******************************************************************************/
#ifndef FNET_CFG_CPU_TIMER_NUMBER
    #define FNET_CFG_CPU_TIMER_NUMBER               (2)
#endif

/******************************************************************************
 *  Vector number of the timer interrupt.
 *  NOTE: User application should not change this parameter. 
 ******************************************************************************/
#ifndef FNET_CFG_CPU_TIMER_VECTOR_NUMBER
    #define FNET_CFG_CPU_TIMER_VECTOR_NUMBER        (XPAR_XTTCPS_0_INTR  + FNET_CFG_CPU_TIMER_NUMBER)
#endif

/******************************************************************************
 *  Vector number of the Ethernet Receive Frame vector number.
 *  NOTE: User application should not change this parameter. 
 ******************************************************************************/
#ifndef FNET_CFG_CPU_ETH0_VECTOR_NUMBER
    #define FNET_CFG_CPU_ETH0_VECTOR_NUMBER        (XPAR_PS7_ETHERNET_0_INTR)
#endif

/******************************************************************************
 *  Vector number of the Ethernet Receive Frame vector number.
 *  NOTE: User application should not change this parameter.
 ******************************************************************************/
#ifndef FNET_CFG_CPU_ETH1_VECTOR_NUMBER
#ifdef XPAR_PS7_ETHERNET_1_INTR
    #define FNET_CFG_CPU_ETH1_VECTOR_NUMBER        (XPAR_PS7_ETHERNET_1_INTR)
#endif
#endif

/******************************************************************************
 * Have second Ethernet Module?
 ******************************************************************************/

#ifndef FNET_CFG_CPU_ETH0
#define FNET_CFG_CPU_ETH0 1
#endif

#ifndef FNET_CFG_CPU_ETH1
#define FNET_CFG_CPU_ETH1 0
#endif

/*****************************************************************************
 *  Enable hardware acceleration of checksum calculation
 ******************************************************************************/

#ifndef FNET_CFG_CPU_ETH_HW_TX_PROTOCOL_CHECKSUM
    #define FNET_CFG_CPU_ETH_HW_TX_PROTOCOL_CHECKSUM    (1)
#endif
#ifndef FNET_CFG_CPU_ETH_HW_TX_IP_CHECKSUM
    #define FNET_CFG_CPU_ETH_HW_TX_IP_CHECKSUM          (1)
#endif
#ifndef FNET_CFG_CPU_ETH_HW_RX_PROTOCOL_CHECKSUM
    #define FNET_CFG_CPU_ETH_HW_RX_PROTOCOL_CHECKSUM    (1)
#endif
#ifndef FNET_CFG_CPU_ETH_HW_RX_IP_CHECKSUM
    #define FNET_CFG_CPU_ETH_HW_RX_IP_CHECKSUM          (1)
#endif

/*****************************************************************************
 *  Byte order is little endian. 
 ******************************************************************************/ 
#undef FNET_CFG_CPU_LITTLE_ENDIAN
#define FNET_CFG_CPU_LITTLE_ENDIAN                  (1)

/*****************************************************************************
 *  On-chip Flash memory start address. 
 ******************************************************************************/ 
#ifndef FNET_CFG_CPU_FLASH_ADDRESS 
    #define FNET_CFG_CPU_FLASH_ADDRESS              (0x0)
#endif 

/*****************************************************************************
 *   On-chip SRAM memory start address. 
 ******************************************************************************/ 
#ifndef FNET_CFG_CPU_SRAM_SIZE
    #define FNET_CFG_CPU_SRAM_SIZE (XPAR_PS7_RAM_0_S_AXI_HP3_HIGHOCM_HIGHADDR - XPAR_PS7_RAM_0_S_AXI_HP3_HIGHOCM_BASEADDR + 1)
#endif

#ifndef FNET_CFG_CPU_SRAM_ADDRESS 
    #define FNET_CFG_CPU_SRAM_ADDRESS   (XPAR_PS7_RAM_0_S_AXI_HP3_HIGHOCM_BASEADDR)
#endif

#ifndef FNET_CFG_CPU_FLASH_PROGRAM_SIZE
    #define FNET_CFG_CPU_FLASH_PROGRAM_SIZE         (0)
#endif 

/*****************************************************************************
 *   Static heap size
 ******************************************************************************/
#ifndef FNET_CFG_HEAP_SIZE
    #define FNET_CFG_HEAP_SIZE              (128 * 1024)
#endif


/* Defines the maximum number of incoming frames that may
 *           be buffered by the Ethernet module.*/
#ifndef FNET_CFG_CPU_ETH_RX_BUFS_MAX
    #define FNET_CFG_CPU_ETH_RX_BUFS_MAX    (48)
#endif

#ifndef FNET_CFG_CPU_ETH_TX_BUFS_MAX
    #define FNET_CFG_CPU_ETH_TX_BUFS_MAX    (48)
#endif


#endif /* FNET_XC7Z */

#endif /* _FNET_XC7Z_CONFIG_H_ */
